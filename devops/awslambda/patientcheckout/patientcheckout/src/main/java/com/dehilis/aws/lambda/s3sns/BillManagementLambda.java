package com.dehilis.aws.lambda.s3sns;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BillManagementLambda  {

	ObjectMapper objectMapper = new ObjectMapper();

	public void handler(SNSEvent event,Context context) {
		LambdaLogger logger= context.getLogger(); 
		event.getRecords().forEach(snsRecord -> {
			try {
				PatientCheckoutEvent patientCheckoutEvent = objectMapper.readValue(snsRecord.getSNS().getMessage(),
						PatientCheckoutEvent.class);
				logger.log(patientCheckoutEvent.toString());
			} catch (JsonProcessingException e) {
				StringWriter stringWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter(stringWriter);
				e.printStackTrace(printWriter);
				logger.log(printWriter.toString());
			}
		});
	}
}
