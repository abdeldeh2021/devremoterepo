package com.dehilis.aws.lambda.s3sns;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;



public class PatientCheckoutLambda {
	
	AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
	private final ObjectMapper objectMapper = new ObjectMapper();
	AmazonSNS sns = AmazonSNSClientBuilder.defaultClient();
	
	
	public void handler(S3Event event,Context context) {
		LambdaLogger logger= context.getLogger(); 
		String  source = event.getRecords().get(0).getS3().getBucket().getName();
		String key = event.getRecords().get(0).getS3().getObject().getKey();
		
		logger.log("Start reading file from S3" + source + "::" +key);
		event.getRecords().forEach(record->{
			
			S3ObjectInputStream s3ObjectInputStream = s3.getObject(record.getS3().getBucket().getName(), record.getS3().getObject().getKey()).getObjectContent();
			try {
				List<PatientCheckoutEvent> patientEvents = Arrays.asList( objectMapper.readValue(s3ObjectInputStream , PatientCheckoutEvent[].class));
				logger.log("---"+ patientEvents.toString());
				patientEvents.forEach(chekoutEvent->{
					
						logger.log("publish to topic " + System.getenv("PATIENT_CHECKOUT_TOPIC"));
						try {
							logger.log("publishing>>" + objectMapper.writeValueAsString(chekoutEvent));
							sns.publish(System.getenv("PATIENT_CHECKOUT_TOPIC"), objectMapper.writeValueAsString(chekoutEvent));
							
						} catch (JsonProcessingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
								
				});
				s3ObjectInputStream.close();
				
					
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				StringWriter stringWriter = new StringWriter();
				PrintWriter printWriter = new PrintWriter(stringWriter);
				e.printStackTrace(printWriter);
				logger.log(printWriter.toString());
				
			} catch (StreamReadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatabindException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				logger.log("Elias" + source+"-traited");
				s3.copyObject(source, key,source + "-traited", key);
			}
		});
	}

}
