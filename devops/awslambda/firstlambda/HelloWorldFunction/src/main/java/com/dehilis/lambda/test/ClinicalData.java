package com.dehilis.lambda.test;

public class ClinicalData {
    private String temp;
    private Integer roomNumber;

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public Integer getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Integer roomNumber) {
        this.roomNumber = roomNumber;
    }

    @Override
    public String toString() {
        return "ClinicalData{" +
                "temp='" + temp + '\'' +
                ", roomNumber=" + roomNumber +
                '}';
    }
}
