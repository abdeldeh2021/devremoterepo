package com.dehilis.lambda.test;

/**
 * Handler for requests to Lambda function.
 */
public class App {

    public String  hello() {
       return "hello Elias";
    }

    public  ClinicalData getClinicalData(Patient patient){
        System.out.println(patient);
        ClinicalData clinicalData =  new ClinicalData();
        clinicalData.setTemp("29");
        clinicalData.setRoomNumber(200);
        return clinicalData;

    }

}
