package com.dehilis.lambda.test;

public class Patient {
    @Override
    public String toString() {
        return "Patient{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", pId='" + pId + '\'' +
                '}';
    }

    private String firstName;
    private String lastName;
    private String pId;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getpId() {
        return pId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }
}

