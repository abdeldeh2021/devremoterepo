import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { UserModel } from './interface/user-model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedIn$ = new BehaviorSubject<boolean>(false);
  isLoggedInAs$ = this.isLoggedIn$.asObservable();
  private readonly TOKEN_NAME = 'elias';
  user:UserModel| null;

  constructor(private userService: UserService) {
      const token= localStorage.getItem('elias');
      this.isLoggedIn$.next(!!token);
   }

  login(loginId: string, password: string) {
    return this.userService.login(loginId, password).subscribe({

      next: (res: any) => { console.log('elias res>>>>' + res.token); localStorage.setItem(this.TOKEN_NAME, res.token) ;
       this.isLoggedIn$.next(true);

       this.user = this.getUser(res.token);console.log("Elias>>>" + JSON.stringify(this.user));
       localStorage.setItem('currentUser', JSON.stringify(this.user));
      },

      error: (err) => {console.log("error:" + err.message);
        this.handleError(err);
      }
    }

    );

  }
  get token(): any {
    console.log('restore token'+localStorage.getItem(this.TOKEN_NAME));
    return localStorage.getItem(this.TOKEN_NAME);
  }
  private getUser(token: string): UserModel | null {
    if (!token) {
      return null
    }
    return JSON.parse(atob(token.split('.')[1])) as UserModel;
  }
  logoutUser(){
    this.isLoggedIn$.next(false);
    localStorage.removeItem('elias');
  }
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      if (error.status === 500) {
        errorMessage = "Username or password wrong"
      } else {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }

    }

    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}

