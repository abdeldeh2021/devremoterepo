import { ErrorHandler, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable,throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Employee } from './interface/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService  implements ErrorHandler{

  URL_BASE= environment.apiURL;
  constructor(private httpClient: HttpClient) { }
 
  findAllPerPage(page: number, size: number): Observable<any> {
    let params = new HttpParams();
    params = params.append("page", String(page));
    params = params.append("size", String(size));
    return this.httpClient.get<any>(this.URL_BASE + '/v1/api/employee', { params }).pipe(retry(1), catchError(this.handleError));
  }
  createEmployee(employee:Employee): Observable<any>{
    return this.httpClient.post<any>(this.URL_BASE + '/v1/api/employee',employee).pipe(retry(1), catchError(this.handleError));
  
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      if(error.status===200) {
        errorMessage="deleted ..."
      }else {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }

    }

    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
