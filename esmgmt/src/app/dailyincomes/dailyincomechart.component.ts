import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChartConfiguration, ChartDataset, ChartOptions, Chart, registerables } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';
import { DailyincomesService } from '../dailyincomes.service';
import { DailyIncome } from '../interface/daily-income';
import { DailyIncomeData } from '../interface/daily-income-data';
import { DatePipe } from '@angular/common'

@Component({
  selector: 'app-dailyincomechart',
  templateUrl: './dailyincomechart.component.html',
  styleUrls: ['./dailyincomechart.component.scss']
})
export class DailyincomechartComponent implements OnInit {

  @ViewChild(BaseChartDirective) chart_y: BaseChartDirective;
  @ViewChild('canvas') canvas: ElementRef;
  context: CanvasRenderingContext2D;


  public charData: ChartDataset[] = [
    { data: [], label: "Data 1", backgroundColor: '#88B5E83', borderRadius: 20 },
    { data: [], label: "Data 2", backgroundColor: '#88B5E83', borderRadius: 20 }
  ];
  title = 'ng2-charts-demo';
  dataSource: DailyIncomeData;
  chart: Chart;

  //public chardData: ChartDataset[] = [{},]
  public lineChartData: ChartConfiguration<'line'>['data'] = {
    labels: ['January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July'],
    datasets: [
      {
        data: [],
        label: 'Series A',
        backgroundColor: 'rgba(77,83,96,0.2)',
        borderColor: 'rgba(77,83,96,1)',
        pointBackgroundColor: 'rgba(77,83,96,1)',
        pointBorderColor: '#fff',
        pointHoverBackgroundColor: '#fff',
        pointHoverBorderColor: 'rgba(77,83,96,1)',
        fill: 'origin',
      }
    ]
  };
  public lineChartOptions: ChartOptions<'line'> = {
    responsive: true,

  };
  public lineChartLegend = true;
  public lineChartPlugins = [];
  constructor(private DdilyincomeserviceService: DailyincomesService) {
    Chart.register(...registerables);

  }
  setData(dataSource: DailyIncomeData) {
    this.dataSource = dataSource;
    console.log(">>>>>Elias" + this.dataSource.dailyIncomes[0].buildDate);
  }
  ngOnInit(): void {
    this.DdilyincomeserviceService.getAll().subscribe(res => {
      this.dataSource = res;
      var data = new Array();
      var cols = new Array();
      // for(var val of res.dailyIncomes) {
      //   cols.push(val.buildDate);
      //   data.push(val.dailyIncome);
      // }

      res.dailyIncomes.slice().reverse().forEach(function ( item: DailyIncome) {
       
       
        cols.push(item.buildDate);
        data.push(item.dailyIncome);
      });
      //this.lineChartData.datasets[0].data=[10,9,8,7];
      //show Chart
      this.context = this.canvas.nativeElement.getContext('2d');
      let gradient = this.context.createLinearGradient(0, 0, 0, 200);
      //gradient.addColorStop(0, 'green');
      //gradient.addColorStop(1, 'white');
      this.chart = new Chart(this.context, {
        type: 'line',
        data: {
          labels: cols,
          datasets: [{
            data: data,
            borderWidth: 4,
            label: 'entree journalieres',
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#8edd39',
            pointHoverBackgroundColor: '#8edd39',
            pointHoverBorderColor: 'rgba(77,83,96,1)',
            fill: 'origin',


          }]
        },
        options: {
         
        }

      });
    })
  }
  initDataSource() {
  }

}
