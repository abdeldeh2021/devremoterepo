import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table/table';
import { DailyIncome } from '../interface/daily-income';
import { DailyIncomeData } from '../interface/daily-income-data';
import { DailyincomesService } from '../dailyincomes.service';
import { catchError, map, timeout } from 'rxjs/operators';

@Component({
  selector: 'app-dailyincomes',
  templateUrl: './dailyincomes.component.html',
  styleUrls: ['./dailyincomes.component.scss']
})
export class DailyincomesComponent implements OnInit {

  dataSource: DailyIncomeData;
  incomes : DailyIncome[];
   // dataSource: MatTableDataSource<MemberModel>;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild('grid') public grid: MatTable<DailyIncome> ;
  constructor(private dailyincomesService:DailyincomesService) {
    console.log(">>>>")
    this.incomes=[]
    
   }

  ngOnInit(): void {
    this.initDataSource();
  }
  initDataSource() {
    this.dailyincomesService.getAll().pipe(
      map((dailyIcomeData: DailyIncomeData)=>this.incomes=dailyIcomeData.dailyIncomes)

    ).subscribe();
  }

}
