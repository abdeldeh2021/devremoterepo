import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomestatComponent } from './dailyincomestat.component';

describe('DailyincomestatComponent', () => {
  let component: DailyincomestatComponent;
  let fixture: ComponentFixture<DailyincomestatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomestatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomestatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
