import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomesComponent } from './dailyincomes.component';

describe('DailyincomesComponent', () => {
  let component: DailyincomesComponent;
  let fixture: ComponentFixture<DailyincomesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
