import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomechartComponent } from './dailyincomechart.component';

describe('DailyincomechartComponent', () => {
  let component: DailyincomechartComponent;
  let fixture: ComponentFixture<DailyincomechartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomechartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
