import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DailyincomesRoutingModule } from './dailyincomes-routing.module';
import { DailyincomesComponent } from './dailyincomes.component';
import { MatTableModule } from '@angular/material/table';
import { DashboradincomeComponent } from './dashboradincome/dashboradincome.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import { DailyincomechartComponent } from './dailyincomechart.component';
import { NgChartsModule } from 'ng2-charts';
import { DailyincomestatComponent } from './dailyincomestat.component';
import { AuthInterceptorProvider } from '../utils/AuthInterceptorService';



@NgModule({
  declarations: [
    DailyincomesComponent,
    DashboradincomeComponent,
    DailyincomechartComponent,
    DailyincomestatComponent,
  
  ],
  imports: [
    CommonModule,
    DailyincomesRoutingModule,
    MatTableModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    NgChartsModule,
  ],
  exports: [
    DailyincomechartComponent,
    DailyincomestatComponent,
   
  ] ,providers: [AuthInterceptorProvider],
})
export class DailyincomesModule { }
