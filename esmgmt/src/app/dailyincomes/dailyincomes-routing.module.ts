import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DailyincomesComponent } from './dailyincomes.component';
import { DashboradincomeComponent } from './dashboradincome/dashboradincome.component';


const routes: Routes = [{ path: '', component: DashboradincomeComponent },{ path: '/dashboard', component: DashboradincomeComponent  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyincomesRoutingModule { }
