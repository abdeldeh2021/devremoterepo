import { Component, OnInit,ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table/table';
import { Incomestat } from '../interface/incomestat';
import { Incomestatlist } from '../interface/incomestatlist';
import { DailyincomesService } from '../dailyincomes.service';
import { catchError, map, timeout } from 'rxjs/operators';

@Component({
  selector: 'app-dailyincomestat',
  templateUrl: './dailyincomestat.component.html',
  styleUrls: ['./dailyincomestat.component.scss']
})
export class DailyincomestatComponent implements OnInit {

  dataSource: Incomestatlist;
  incomes: Incomestat[];
   // dataSource: MatTableDataSource<MemberModel>;
  // @ViewChild(MatPaginator) paginator: MatPaginator;
   @ViewChild('grid') public grid: MatTable<Incomestat> ;
  constructor(private dailyincomesService:DailyincomesService) {
    this.incomes=[];
  }
  ngOnInit(): void {
    this.initDataSource();
    console.log(this.incomes.length);
  }
  initDataSource() {
    this.dailyincomesService.getStat().pipe(
      map((datasourceStat: any)=>this.incomes =datasourceStat)
    ).subscribe();
  }
}
