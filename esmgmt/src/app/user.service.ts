import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from  './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  URL_BASE = environment.apiURL;
  constructor(private http: HttpClient) { }

  login(userName: string, password: string) {
    return this.http.post(this.URL_BASE + '/authenticate/signIn', { userName, password })
  }
}
