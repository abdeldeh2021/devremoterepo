import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomedialogComponent } from './dailyincomedialog.component';

describe('DailyincomedialogComponent', () => {
  let component: DailyincomedialogComponent;
  let fixture: ComponentFixture<DailyincomedialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomedialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
