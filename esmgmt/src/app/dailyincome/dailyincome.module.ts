import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';
import { DailyincomedialogComponent } from './dailyincomedialog.component';



@NgModule({
  declarations: [
    ListComponent,
    DailyincomedialogComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ListComponent,
    DailyincomedialogComponent
  ]
})
export class DailyincomeModule { }
