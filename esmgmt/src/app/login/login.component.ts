import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {Router, RouterLinkWithHref} from '@angular/router';
import {AuthService} from '../auth.service';
import {UserService} from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginId: String;
  password: String;
  showMess:boolean=false;

  form = this.fb.group({
    userName: ['', [Validators.required, Validators.maxLength(60)]],
    password: ['', [Validators.required, Validators.maxLength(60)]],

  });

  constructor(private router: Router, private fb: FormBuilder, public authService: AuthService) {
    this.showMess=false;
  }

  ngOnInit(): void {
    this.showMess=false;
  }

  validateUser() {
    this.showMess=false;
    if (this.form.invalid) {
      return;
    }
    this.authService.login(this.form.get('userName')?.value, this.form.get('password')?.value)
    this.authService.isLoggedInAs$.subscribe((e)=>{
      if(e) {
        this.showMess=false;
        this.router.navigate(['/dailyincomes']);
      }else{
        this.showMess=true;
      }
    })
  }

  public hasError = (controlName: string, errorName: string) => {
    this.showMess=false;
    return this.form.controls[controlName].hasError(errorName);
  }


}
