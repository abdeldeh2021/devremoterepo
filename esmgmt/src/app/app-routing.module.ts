import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './core/home.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [

  {path:'',component: LoginComponent},
  { path: 'dailyincomes', loadChildren: () => import('./dailyincomes/dailyincomes.module').then(m => m.DailyincomesModule) },
 
  { path: 'materiales', loadChildren: () => import('./materials/materials.module').then(m => m.MaterialsModule) },
 
  { path: 'dailyincomesdata', loadChildren: () => import('./dailyincomesdata/dailyincomesdata.module').then(m => m.DailyincomesdataModule) },
 
  { path: 'matcosts', loadChildren: () => import('./matcosts/matcosts.module').then(m => m.MatcostsModule) },

  { path: 'employees', loadChildren: () => import('./employees/employees.module').then(m => m.EmployeesModule) }

 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
