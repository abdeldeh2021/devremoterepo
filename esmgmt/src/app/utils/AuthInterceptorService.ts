import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HTTP_INTERCEPTORS } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { catchError, Observable, throwError } from "rxjs";
import { AuthService } from "../auth.service";


@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private authService: AuthService) {} 
 intercept(request: HttpRequest<any>, httpHandler: HttpHandler): Observable<HttpEvent<any>> {  
    const token = this.authService.token;
   console.log('>>>>hereis ' + token);
   if (token) {
     // If we have a token, we set it to the header
     request = request.clone({
        headers:request.headers.set('Authorization', `Bearer ${token}`).set('Access-Control-Allow-Origin','http://localhost:4200')
     });
  }
 console.log(request.headers);
  return httpHandler.handle(request).pipe(
  	catchError((err) => {
   	 if (err instanceof HttpErrorResponse) {
       	 if (err.status === 401) {
       	 // redirect user to the logout page
     	}
 	 }
  	return throwError(err);
	})
   )
  }
}
interface HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
   }
 export const AuthInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true
 }  