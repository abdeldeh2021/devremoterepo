import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcostsComponent } from './matcosts.component';

describe('MatcostsComponent', () => {
  let component: MatcostsComponent;
  let fixture: ComponentFixture<MatcostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatcostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
