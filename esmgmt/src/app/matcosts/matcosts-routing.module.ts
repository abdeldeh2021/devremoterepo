import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatcostsComponent } from './matcosts.component';

const routes: Routes = [{ path: '', component: MatcostsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MatcostsRoutingModule { }
