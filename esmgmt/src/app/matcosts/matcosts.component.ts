import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatCostServiceService } from '../mat-cost-service.service';
import { CostData } from './interface/cost-data';
import { MatCost } from './interface/mat-cost';
import {finalize, map} from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';
import {Router, RouterLinkWithHref} from '@angular/router';
import { MaterialsService } from '../materials.service';
import { MaterialType } from '../interface/material-type';
import { MaterialItem } from '../interface/material-item';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-matcosts',
  templateUrl: './matcosts.component.html',
  styleUrls: ['./matcosts.component.scss'],
  providers: [DatePipe],
})
export class MatcostsComponent implements OnInit {

  form = this.fb.group({
    amount: ['', [Validators.required, Validators.maxLength(60)]],
    description: ['', [Validators.required, Validators.maxLength(60)]],
    costDate: [''],
    matCostType: [''],
    materiel: [{}],
    userName:['test'],
    type_id:['BEGINNER', Validators.required],
  });
  pageEvent: PageEvent;
  dataSource: CostData;
  mattypes: any;
  materials: MaterialItem[];
  matcosttypes =['Essence','Pieces','Divers'];
  loading = false;
  constructor(private router: Router,private fb: FormBuilder,
               private matCostServiceService: MatCostServiceService,
                private materialsService: MaterialsService,
              private myx:DatePipe) {



  }

  ngOnInit(): void {
    this.materialsService.getAllMaterieltype().subscribe(mattypes=>this.mattypes= mattypes);
    this.loading=true;
    setTimeout(() => {   this.initDataSource(); }, 2000);
  }
  initDataSource() {
   this.matCostServiceService.findAllPerPage(0, 10).pipe(map((dataSource:CostData)=>{this.dataSource=dataSource, console.log(JSON.stringify(this.dataSource))} ), finalize(()=>{this.loading=false})).subscribe()

  }
  addMatObject(data: MatCost) {

      //console.log(new Date(data.costDate).)

      console.log(data);
      this.matCostServiceService.saveMatCost(data).subscribe(res=>this.initDataSource());
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
  onPaginateChange(event: PageEvent) {
    let page = event.pageIndex;
    let size = event.pageSize;
    this.matCostServiceService.findAllPerPage(page, size).pipe(
      map((costdata: CostData) => this.dataSource = costdata)
    ).subscribe();

  }

  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {

    const date = cellDate.getDate();

    if (view == 'month') {
      return (date == 1) ? 'highlight-date' : "";
    }

    return "";
  }
  onChange(matType: MaterialType) {

    console.log(matType);
    this.materialsService.getAllMaterielBytype(matType).subscribe(materels => { this.materials = materels; console.log(this.materials.length); console.log(materels) })

}

}
