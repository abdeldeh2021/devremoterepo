import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatcostsRoutingModule } from './matcosts-routing.module';
import { MatcostsComponent } from './matcosts.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';

import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from '@angular/material/dialog';

import {MatNativeDateModule} from '@angular/material/core';
import {  MatPaginatorModule } from '@angular/material/paginator';
import { MatCardModule } from '@angular/material/card';
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


@NgModule({
  declarations: [
    MatcostsComponent
  ],
    imports: [
        MatcostsRoutingModule,
        CommonModule,
        MatIconModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatTableModule,
        MatButtonModule,
        FormsModule,
        MatSelectModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatInputModule,
        MatDatepickerModule,

        MatNativeDateModule,
        MatDialogModule,
        MatPaginatorModule,
        MatCardModule,
        MatProgressSpinnerModule,
    ]
})
export class MatcostsModule { }
