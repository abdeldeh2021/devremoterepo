import { MatCost } from "./mat-cost";

export interface CostData {
    matCosts: MatCost[],
    totalPages: number,
    pageNumber: number,
    totalAmount: number,
    itemsPerPage: number,
    itemCount: number
}
