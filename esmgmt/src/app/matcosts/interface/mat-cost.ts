import { MaterialItem } from "src/app/interface/material-item";

export class MatCost {
    id:number;
    description: String;
    costDate: Date;
    amount: number;
    matname: String;
    materiel: MaterialItem;
    matCostType:String;
    matId: number;
}
