import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { DailyincomeModule } from './dailyincome/dailyincome.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import  { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgChartsModule } from 'ng2-charts';
import { MaterialsModule } from './materials/materials.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LoginComponent } from './login/login.component';


import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import {  AuthInterceptorProvider, AuthInterceptorService } from './utils/AuthInterceptorService';
import { AuthService } from './auth.service';
import {MatIconModule} from '@angular/material/icon';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    DailyincomeModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgChartsModule,
    MaterialsModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule, BrowserModule, FormsModule ,MatButtonModule,MatIconModule,MatProgressSpinnerModule


  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
  bootstrap: [AppComponent],
  exports: [

  ]
})
export class AppModule { }
