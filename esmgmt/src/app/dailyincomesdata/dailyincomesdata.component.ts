import { Component, OnInit ,ChangeDetectorRef} from '@angular/core';
import { DailyincomesService } from '../dailyincomes.service';
import { DailyIncomeData } from '../interface/daily-income-data';
import { startWith, map, switchMap, tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { PageEvent } from '@angular/material/paginator';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MatCalendarCellClassFunction } from '@angular/material/datepicker';
import {Router, RouterLinkWithHref} from '@angular/router';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DailyincomeconfirmdialogComponent} from './dailyincomeconfirmdialog/dailyincomeconfirmdialog.component';
import { DailyIncome } from '../interface/daily-income';
import { DailyeditdialogComponent } from './dailyeditdialog/dailyeditdialog.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-dailyincomesdata',
  templateUrl: './dailyincomesdata.component.html',
  styleUrls: ['./dailyincomesdata.component.scss']
})
export class DailyincomesdataComponent implements OnInit {

  pageEvent: PageEvent;
  dataSource: DailyIncomeData;
  form = this.fb.group({
    dailyIncome: ['', [Validators.required, Validators.maxLength(60)]],
    dailyReport: ['', [Validators.required, Validators.maxLength(60)]],
    buildDate: [new Date()],
    userName:['test']

  });
  insertDate:Date = new Date();

  constructor(private datePipe: DatePipe,public dialog: MatDialog,private router: Router,private fb: FormBuilder, private dailyIncomesService: DailyincomesService,  private changeDetectorRefs: ChangeDetectorRef) {
     this.initDataSource();
  }

  ngOnInit(): void {
    
  }
  initDataSource() {
    this.dailyIncomesService.findAllPerPage(0, 10).pipe(map((dataSource:DailyIncomeData)=>{this.dataSource=dataSource} )).subscribe();
 
  }
  onPaginateChange(event: PageEvent) {
    let page = event.pageIndex;
    let size = event.pageSize;
    this.dailyIncomesService.findAllPerPage(page, size).pipe(
      map((dailyIncomeData: DailyIncomeData) => this.dataSource = dailyIncomeData)
    ).subscribe();

  }
   addMatObject(data: FormData) {
    // let insertDateStr = this.datePipe.transform(this.insertDate, 'yyyy-MM-dd');
    // data.append('insertDate',insertDateStr);
   // console.log('Data>>>>'+ JSON.stringify(data));
    this.dailyIncomesService.saveDailyIncome(data).subscribe((data: {}) => {this.initDataSource();
     // this.router.navigate(['/dailyincomesdata']);
     } );
   
  }
  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {
    
    const date = cellDate.getDate();

    if (view == 'month') {
      return (date == 1) ? 'highlight-date' : "";
    }

    return "";
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
  
  openDialog(action:String, row:DailyIncome) {
    console.log(action);
     if(action==='Delete') {
      
        this.deleteAction(row);
    }else {
      console.log(action);
      this.updateAction(row);
     }
        
  }
  deleteAction(row:DailyIncome){
    const dialogRef = this.dialog.open(DailyincomeconfirmdialogComponent,{
      data:{
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Delete',
          cancel: 'No'
        }
      }
    });
    
    dialogRef.afterClosed().subscribe({
      next:(confirmed: boolean) => {
        this.dailyIncomesService.deleteDailyIncome(row).subscribe(
          {
            next: res=> {console.log(res.response); this.initDataSource()}
          }
        )
      },
      error:err=>console.log('dehilis' +err)
      }
  );
  }
  updateAction(row:DailyIncome){
    console.log(">>>>" +row.id);
    const config = new MatDialogConfig();
    config.disableClose=true;
    config.autoFocus=true;
    config.data = {
      ...row
    }
    const dialogRef = this.dialog.open(DailyeditdialogComponent,config);
    
          //   dialogRef.afterClosed().subscribe({
          //     next:(confirmed: boolean) => {
          //       this.dailyIncomesService.deleteDailyIncome(row).subscribe(
          //         {
          //           next: res=> {console.log(res.response); this.initDataSource()}
          //         }
          //       )
          //     },
          //     error:err=>console.log('dehilis' +err)
          //     }
          // );
  }
}


