import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyeditdialogComponent } from './dailyeditdialog.component';

describe('DailyeditdialogComponent', () => {
  let component: DailyeditdialogComponent;
  let fixture: ComponentFixture<DailyeditdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyeditdialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyeditdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
