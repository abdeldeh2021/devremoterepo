import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DailyIncome } from 'src/app/interface/daily-income';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";

@Component({
  selector: 'app-dailyeditdialog',
  templateUrl: './dailyeditdialog.component.html',
  styleUrls: ['./dailyeditdialog.component.scss']
})
export class DailyeditdialogComponent implements OnInit {


  form = this.fb.group({
    dailyIncome: [this.dailyIncome.dailyIncome, Validators.required],
    dailyReport: [this.dailyIncome.dailyReport, Validators.required],
    buildDate: [this.dailyIncome.buildDate,Validators.required]
  });
  constructor(private fb: FormBuilder, @Inject(MAT_DIALOG_DATA) private dailyIncome: DailyIncome, private dialogRef: MatDialogRef<DailyeditdialogComponent>) {
       console.log(this.dailyIncome.buildDate)
  }
  ngOnInit(): void {
  }
  close() {

    this.dialogRef.close();

  }

  save() {

    this.dialogRef.close(this.form.value);

  }
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
}
