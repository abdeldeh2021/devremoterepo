import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomeconfirmdialogComponent } from './dailyincomeconfirmdialog.component';

describe('DailyincomeconfirmdialogComponent', () => {
  let component: DailyincomeconfirmdialogComponent;
  let fixture: ComponentFixture<DailyincomeconfirmdialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomeconfirmdialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomeconfirmdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
