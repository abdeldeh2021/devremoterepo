import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dailyincomeconfirmdialog',
  templateUrl: './dailyincomeconfirmdialog.component.html',
  styleUrls: ['./dailyincomeconfirmdialog.component.scss']
})
export class DailyincomeconfirmdialogComponent implements OnInit {

  message: string = "Are you sure?"
  confirmButtonText = "Yes"
  cancelButtonText = "Cancel"
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<DailyincomeconfirmdialogComponent>) {
    if (data) {
      this.message = data.message || this.message;
      if (data.buttonText) {
        this.confirmButtonText = data.buttonText.ok || this.confirmButtonText;
        this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
      }
    }
  }
  ngOnInit(): void {

  }

  onConfirmClick(): void {
    console.log(' confirmed');
    this.dialogRef.close(true);
  }


}
