import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyincomesdataComponent } from './dailyincomesdata.component';

describe('DailyincomesdataComponent', () => {
  let component: DailyincomesdataComponent;
  let fixture: ComponentFixture<DailyincomesdataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DailyincomesdataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyincomesdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
