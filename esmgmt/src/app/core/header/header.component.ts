import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { UserModel } from 'src/app/interface/user-model';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  
})
export class HeaderComponent implements OnInit {

  name:string | undefined; 
  constructor(public router: Router, public authService:AuthService) { };
  currentUser: UserModel;

  ngOnInit(): void {
    this.currentUser =  JSON.parse(localStorage.getItem('currentUser')|| '{}');
    this.name= this.currentUser.sub;
   
  }
  logoutUser(){
    this.authService.logoutUser();
    this.router.navigate(['']);
  }

}
