import { Component, OnInit, Inject  } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import {MatCalendarCellClassFunction} from '@angular/material/datepicker';
@Component({
  selector: 'app-employeedialog',
  templateUrl: './employeedialog.component.html',
  styleUrls: ['./employeedialog.component.scss']
})
export class EmployeedialogComponent implements OnInit {

  message: string = "Are you sure?"
  confirmButtonText = "Yes"
  cancelButtonText = "Cancel"

  form = this.fb.group({
   firstName: ['', Validators.required],
   lastName: ['', Validators.required],
   assur: ['', Validators.required],
   phoneNumber: [null, [Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
   dateOfBirth: ['', Validators.required],
  });

  constructor(private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<EmployeedialogComponent>) {
    if (data) {
      this.message = data.message || this.message;
      if (data.buttonText) {
        this.confirmButtonText = data.buttonText.ok || this.confirmButtonText;
        this.cancelButtonText = data.buttonText.cancel || this.cancelButtonText;
      }
    }
  }
  
  ngOnInit(): void {
    
  }

 
  close() {
    this.dialogRef.close(false);
  }

  save() {
    this.dialogRef.close(this.form.value);
  }
  public hasError = (controlName: string, errorName: string) => {
    return this.form.controls[controlName].hasError(errorName);
  }
  

  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {

    const date = cellDate.getDate();
    if (view == 'month') {
        return (date == 1) ? 'highlight-date' : "";
    }
    return "";
}
}
