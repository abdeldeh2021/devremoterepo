import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../employee.service';
import {finalize, map} from 'rxjs/operators';
import { Employee } from '../interface/employee';
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EmployeedialogComponent } from './employeedialog/employeedialog.component';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  dataSource:any;
  isLoading: boolean=false;
  constructor(private employeeService:EmployeeService,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.initDataSource();
  }
  initDataSource() {
    this.employeeService.findAllPerPage(0, 10).subscribe
      ({
        next: (res) => {
          this.isLoading = false;
         // this.onSuccess(res.body, res.headers);
         this.dataSource=res;
        },
        error: () => {
          this.isLoading = false;
          this.onError();
        },
      });
   }
   protected onSuccess(data: any | null, headers: HttpHeaders): void {
    this.dataSource = data ;
   
  }
  protected onError(): void {
    console.log('Error loading data...');
  }
  // pipe(map((employees)=>{this.dataSource=employees, console.log(JSON.stringify(this.dataSource))} )).subscribe()

   openDialog(action:String, row:Employee) {
    console.log(action);
     if(action==='Delete') {
        this.deleteAction(row);
    }else {
      console.log(action);
      this.updateAction(row);
     }     
  }
  openDialogAdd(){

    const dialogRef = this.dialog.open(EmployeedialogComponent,{
      data:{
        message: 'Add new employee',
        buttonText: {
          ok: 'Save',
          cancel: 'No'
        }
      }
    });
    dialogRef.afterClosed().subscribe({
      next:(res:any) => {
         if(res!=false) {
         console.log(res);
          this.employeeService.createEmployee(res).subscribe(res=>{console.log(res);this.initDataSource()});
        } 
      },
      error:err=>console.log('dehilis' +err)
      }
  );
  }
  deleteAction(row:Employee){
    const dialogRef = this.dialog.open(EmployeedialogComponent,{
      data:{
        message: 'Are you sure want to delete?',
        buttonText: {
          ok: 'Delete',
          cancel: 'No'
        }
      }
    });
    
    dialogRef.afterClosed().subscribe({
      next:(confirmed: boolean) => {
          console.log("Dialog closed");
      },
      error:err=>console.log('dehilis' +err)
      }
  );
  }
  updateAction(row:Employee){
    console.log(">>>>" +row.id);
    const config = new MatDialogConfig();
    config.disableClose=true;
    config.autoFocus=true;
    config.data = {
      ...row
    }
  }

}
