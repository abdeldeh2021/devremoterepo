import { ErrorHandler, Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable,throwError } from 'rxjs';
import { CostData } from './matcosts/interface/cost-data';
import { MatCost } from './matcosts/interface/mat-cost';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MatCostServiceService implements ErrorHandler  {
  URL_BASE= environment.apiURL;
  constructor(private httpClient: HttpClient) { }
  findAllPerPage(page: number, size: number): Observable<CostData> {
    let params = new HttpParams();
    params = params.append("page", String(page));
    params = params.append("size", String(size));
    return this.httpClient.get<CostData>(this.URL_BASE + '/api/matcost', { params }).pipe(retry(1), catchError(this.handleError));

  }
  findAllByMatIdPerPage(page: number, size: number,matId: number): Observable<CostData> {
    let params = new HttpParams();
    params = params.append("page", String(page));
    params = params.append("size", String(size));
    params = params.append("matId", String(matId));
    return this.httpClient.get<CostData>(this.URL_BASE + '/api/matcost/matcostsbyid', { params }).pipe(retry(1), catchError(this.handleError));

  }
  saveMatCost(formData: MatCost): Observable<String> {
   // const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.httpClient.post<any>(this.URL_BASE + '/api/matcost', formData).pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      if(error.status===200) {
        errorMessage="deleted ..."
      }else {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }

    }

    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }
}
