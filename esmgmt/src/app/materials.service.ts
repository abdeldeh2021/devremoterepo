import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable,catchError,throwError, map, timeout } from 'rxjs';
import { MaterialType } from './interface/material-type';
import { MaterialItem } from './interface/material-item';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MaterialsService {

  URL_BASE= environment.apiURL;
  public mattypes: any ;
  mydata:any;

  constructor(private httpClient: HttpClient) {

   }
   //http://localhost:5000/api/materiel/mattypes
   headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Authorizatuin':'testeeee'
  }
   public getAllMaterieltype(){

    return  this.httpClient.get(this.URL_BASE +'/api/materiel/mattypes').pipe(map(res=>{this.mattypes=res; let stringifiedData = JSON.stringify(res);
      console.log("With Stringify :" , stringifiedData);return res;}));
   }

  //http://localhost:5000/api/materiel/mattypes
  public getAllMaterielBytype( mattype:MaterialType):Observable<MaterialItem[]>{
    let params = new HttpParams().set('mattype', encodeURIComponent(JSON.stringify( mattype)));
    return this.httpClient.get<MaterialItem[]>(this.URL_BASE +'/api/materiel/matbytype/'+mattype.id)
   }
   registerUser(formData : FormData): void{
    console.log(formData);
    this.httpClient.post<any>(this.URL_BASE +'/api/materiel/creatematerial', formData).subscribe({
      next: (response) => console.log(response),
      error: (error) => console.log(error),
    });

  }
  saveMatType(formData:FormData):void {
    this.httpClient.post<any>(this.URL_BASE +'/api/materiel', formData).subscribe({
      next: (response) => console.log(response),
      error: (error) => console.log(error),
    });
  }
  public getMaterielById(id:number):Observable<any>{
    return  this.httpClient.get(this.URL_BASE +'/api/materiel/' + id).pipe(map(res=>{this.mattypes=res; let stringifiedData = JSON.stringify(res);
      console.log("With Stringify :" , stringifiedData);return res;}));
   }


}
