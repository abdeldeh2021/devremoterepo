import { TestBed } from '@angular/core/testing';

import { DailyincomesService } from './dailyincomes.service';

describe('DailyincomesService', () => {
  let service: DailyincomesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DailyincomesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
