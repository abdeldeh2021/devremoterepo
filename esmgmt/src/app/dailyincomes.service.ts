import { Injectable } from '@angular/core';
import { DailyIncomeData } from './interface/daily-income-data';
import { HttpClient, HttpErrorResponse, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of, TimeoutError } from 'rxjs';
import { retry, catchError, map, timeout } from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatTable } from '@angular/material/table/table';
import { Incomestatlist } from './interface/incomestatlist';
import { Incomestat } from './interface/incomestat';
import { DailyIncome } from './interface/daily-income';
import { environment } from  './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DailyincomesService {
  public datasource: DailyIncomeData;
  datasourceStat: Incomestat[];
  URL_BASE = environment.apiURL;
  message: String;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      
      'responseType': 'text'
    }),
  };
  constructor(private httpClient: HttpClient) {

  }

  getAll() {
    //return this.httpClient.get(this.URL_BASE+ '/api/dailyincome');
    let res = this.httpClient.get<DailyIncomeData>(this.URL_BASE + '/api/dailyincome').pipe(
      retry(1), catchError(this.handleError),
    )
    console.log(">>>>>>" + JSON.stringify(res));
    return res;
  }
  findAllPerPage(page: number, size: number): Observable<DailyIncomeData> {
    let params = new HttpParams();
    params = params.append("page", String(page));
    params = params.append("size", String(size));

    return this.httpClient.get<DailyIncomeData>(this.URL_BASE + '/api/dailyincome', { params }).pipe(retry(1), catchError(this.handleError));

  }
  getStat() {
    //return this.httpClient.get(this.URL_BASE+ '/api/dailyincome');
    return this.httpClient.get<Incomestatlist>(this.URL_BASE + '/api/dailyincome/incomesmounth').pipe(
      map((res: any) => this.datasourceStat = res),
      catchError(err => throwError(err))
    )
  }
  saveDailyIncome(formData: any): Observable<String> {
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    return this.httpClient.post<any>(this.URL_BASE + '/api/dailyincome', formData).pipe(retry(1), catchError(this.handleError));
  }
  deleteDailyIncome(dailyIncome: DailyIncome): Observable<any>{
     console.log('deleted id=' + dailyIncome.id);
    return this.httpClient.delete<any>(this.URL_BASE + '/api/dailyincome/' + dailyIncome.id, this.httpOptions).pipe(
      retry(1), catchError(this.handleError)
    );
  }
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      if(error.status===200) {
        errorMessage="deleted ..."
      }else {
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
     
    } 
    
    window.alert(errorMessage);
    return throwError(() => {
      return errorMessage;
    });
  }

}
