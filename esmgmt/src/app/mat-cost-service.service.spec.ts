import { TestBed } from '@angular/core/testing';

import { MatCostServiceService } from './mat-cost-service.service';

describe('MatCostServiceService', () => {
  let service: MatCostServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatCostServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
