import { DailyIncome } from "./daily-income";
export interface DailyIncomeData {
    dailyIncomes: DailyIncome[],
    totalPages: number,
    pageNumber: number,
    totalAmount: number,
    itemsPerPage: number,
    itemCount: number
}
