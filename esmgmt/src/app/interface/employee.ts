export interface Employee {

    id: number,
    firstName: string,
    lastName: string,
    insertDate: string,
    assur: string,
    dateAssur: Date,
    dateOfBirth: Date,
    address: string,
    emplDate: Date,
    phoneNumber: String,

}
