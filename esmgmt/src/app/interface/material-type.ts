export interface MaterialType {
    id: number;
    name: String;
    description: String;
}
