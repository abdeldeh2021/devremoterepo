export interface DailyIncome {
    id:number;
    userName: String;
    buildDate: Date;
    dailyIncome: number;
    dailyReport: String;
}
