export interface UserModel {
    sub: string;
    firstName: string;
    role: string[];
}
