import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatCostServiceService} from "../../mat-cost-service.service";
import {MaterialItem} from "../../interface/material-item";
import {map} from "rxjs/operators";
import {CostData} from "../../matcosts/interface/cost-data";
import {MatTable} from "@angular/material/table/table";
import { PageEvent } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";

@Component({
  selector: 'app-matcostdetails',
  templateUrl: './matcostdetails.component.html',
  styleUrls: ['./matcostdetails.component.scss']
})
export class MatcostdetailsComponent implements OnInit {

  @Input() matItem: any;
  dataSource: CostData;
  @ViewChild('grid') public grid: MatTable<MaterialItem>;
  pageEvent: PageEvent;

  constructor(private matCostService: MatCostServiceService) {
  //  this.initDataSource();

  }

  ngOnInit(): void {
    console.log("+++" + this.matItem.id);
    this.initDataSource();
  }
  initDataSource() {

   this.matCostService.findAllByMatIdPerPage(0, 5,this.matItem.id).pipe(map((dataSource:CostData)=>{this.dataSource=dataSource, console.log(JSON.stringify(this.dataSource))} )).subscribe();

  }
  onPaginateChange(event: PageEvent) {
    let page = event.pageIndex;
    let size = event.pageSize;
    this.matCostService.findAllByMatIdPerPage(page, size,this.matItem.id).pipe(
      map((costdata: CostData) => this.dataSource = costdata)
    ).subscribe();

  }
}
