import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatcostdetailsComponent } from './matcostdetails.component';

describe('MatcostdetailsComponent', () => {
  let component: MatcostdetailsComponent;
  let fixture: ComponentFixture<MatcostdetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatcostdetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatcostdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
