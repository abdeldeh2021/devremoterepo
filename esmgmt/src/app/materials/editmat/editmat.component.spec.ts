import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditmatComponent } from './editmat.component';

describe('EditmatComponent', () => {
  let component: EditmatComponent;
  let fixture: ComponentFixture<EditmatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditmatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditmatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
