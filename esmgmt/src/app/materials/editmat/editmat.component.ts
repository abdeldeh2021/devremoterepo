import { Component, OnInit,Input } from '@angular/core';
import { MaterialType } from '../../interface/material-type';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import { MaterialsService } from '../../materials.service';
import {MatCalendarCellClassFunction} from '@angular/material/datepicker';


@Component({
  selector: 'app-editmat',
  templateUrl: './editmat.component.html',
  styleUrls: ['./editmat.component.scss']
})
export class EditmatComponent implements OnInit {

  
  @Input()   mattypes: any;
  @Input() test:String;
  projects :any;
  form=this.fb.group({
    name:['',[Validators.required,Validators.maxLength(60)]],
    description:['',[Validators.required,Validators.maxLength(60)]],
    type_id:['BEGINNER', Validators.required],
    assur:['BEGINNER', Validators.required],
    immat:[''],
    isEnabled:[true],
    dateAssur: [new Date(1990,0,1)],
    project_id:['BEGINNER', Validators.required],

  });
  dateClass: MatCalendarCellClassFunction<Date> = (cellDate, view) => {

    const date = cellDate.getDate();

    if (view == 'month') {
        return (date == 1) ? 'highlight-date' : "";
    }

    return "";
}
  constructor(private fb:FormBuilder, private materialsService: MaterialsService) {
    console.log(">>>>lll"+ this.mattypes?.length);
  }

  ngOnInit(): void {
   // this.materialsService.getAllMaterieltype().subscribe(mattypes=>this.mattypes= mattypes)
   this.mattypes= this.materialsService.mattypes;
   console.log(this.mattypes?.length);
 
  }
  addMatObject(data: FormData): void{
    console.log("ets");
    console.log(this.form.value);
    this.materialsService.registerUser(data);
    this.form.reset();
  }
  public hasError = (controlName: string, errorName: string) =>{
    return this.form.controls[controlName].hasError(errorName);
  }
}
