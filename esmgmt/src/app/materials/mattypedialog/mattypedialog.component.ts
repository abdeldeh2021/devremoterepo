import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import {FormBuilder, Validators, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-mattypedialog',
  templateUrl: './mattypedialog.component.html',
  styleUrls: ['./mattypedialog.component.scss']
})
export class MattypedialogComponent implements OnInit {

  description: string;
  form = this.fb.group({
    description: ['', Validators.required],
    name: ['',  Validators.required],
    
 });
  constructor(private fb: FormBuilder,private dialogRef: MatDialogRef<MattypedialogComponent>) { }

  ngOnInit(): void {
    this.description = "Type Materiel"
  }
  close() {

    this.dialogRef.close();

  }

  save() {

    this.dialogRef.close(this.form.value);

  }

}
export function openEditCourseDialog(dialog: MatDialog, course: String) {

  const config = new MatDialogConfig();

  config.disableClose = true;
  config.autoFocus = true;
  config.panelClass = "modal-panel";
  config.backdropClass = "backdrop-modal-panel";

  config.data = {
    ...course
  };

  const dialogRef = dialog.open(MattypedialogComponent, config);

  return dialogRef.afterClosed()
}
