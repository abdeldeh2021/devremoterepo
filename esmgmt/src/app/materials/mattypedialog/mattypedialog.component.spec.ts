import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MattypedialogComponent } from './mattypedialog.component';

describe('MattypedialogComponent', () => {
  let component: MattypedialogComponent;
  let fixture: ComponentFixture<MattypedialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MattypedialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MattypedialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
