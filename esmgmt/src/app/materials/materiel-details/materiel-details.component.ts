import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MaterialsService } from 'src/app/materials.service';

@Component({
  selector: 'app-materiel-details',
  templateUrl: './materiel-details.component.html',
  styleUrls: ['./materiel-details.component.scss']
})
export class MaterielDetailsComponent implements OnInit {

  title:number;
  materielItem:any;
  constructor(private route: ActivatedRoute,private materialsService: MaterialsService) {
   this.initDatasource();
  }

  ngOnInit(): void {

  }
  initDatasource():void {
    const routeParams = this.route.snapshot.paramMap;
    const materialIdFromRoute = Number(routeParams.get('materielId'));
    this.materialsService.getMaterielById(materialIdFromRoute).subscribe({next: res =>{
        this.materielItem=res;

      }});

    this.title = materialIdFromRoute;
    console.log(">>>>>" +this.materielItem);
  }

}
