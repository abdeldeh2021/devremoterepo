import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { map, Observable } from 'rxjs';
import { MaterialItem } from '../interface/material-item';
import { MaterialType } from '../interface/material-type';
import { MaterialsService } from '../materials.service';
import { MatTable } from '@angular/material/table/table';
import { MatButton } from '@angular/material/button';
import { EditmatComponent } from './editmat/editmat.component';
import { ThisReceiver } from '@angular/compiler';
import { MatDialog } from '@angular/material/dialog';




@Component({
  selector: 'app-materials',
  templateUrl: './materials.component.html',
  styleUrls: ['./materials.component.scss']
})
export class MaterialsComponent implements OnInit {
  show: boolean = false;
  public mattypes$: any | undefined;
  materials: MaterialItem[];
  public actualMattype: String = "";
  public matCompType: String = "matType";
  @ViewChild('grid') public grid: MatTable<MaterialItem>;


  constructor(private materialsService: MaterialsService, public dialog: MatDialog) {
    this.materials =  [];
    this.mattypes$ = [];
  }


  ngOnInit(): void {
    this.materialsService.getAllMaterieltype().subscribe(mattypes => this.mattypes$ = mattypes );

  }
  public clickMethod(mattype: MaterialType) {
    this.actualMattype=mattype.name;
    console.log(mattype.id);
    this.matCompType='matObj';
    this.show = false;
    this.materialsService.getAllMaterielBytype(mattype).subscribe(materels => { this.materials = materels; console.log(this.materials.length); console.log(materels) })

  }
  onClick() {
    this.show = !this.show;
  }
  openDialog(action: any, obj: any) {
    console.log("test");

  }
  update(val:any) {
    this.mattypes$=val;
    console.log("parent changed");
  }

}
