import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaterialsComponent } from './materials.component';
import { MaterielDetailsComponent } from './materiel-details/materiel-details.component';

const routes: Routes = [{ path: '', component: MaterialsComponent },{ path: 'materials', component: MaterialsComponent  },
{ path: 'materials/:materielId', component: MaterielDetailsComponent  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaterialsRoutingModule { }
