
import { AfterViewInit, Component, OnInit, ViewChild, ChangeDetectorRef, Input,Output,   EventEmitter } from '@angular/core';

import { MaterialsService } from '../../materials.service';
import { MatTable } from '@angular/material/table/table';
import { MatButton } from '@angular/material/button';
import { filter } from 'rxjs/operators';
import { ThisReceiver } from '@angular/compiler';
import { MatDialog } from '@angular/material/dialog';
import { openEditCourseDialog } from '../mattypedialog/mattypedialog.component';
import {Router} from '@angular/router';
import {MaterialsComponent} from '../materials.component';

@Component({
  selector: 'app-mattypemgmt',
  templateUrl: './mattypemgmt.component.html',
  styleUrls: ['./mattypemgmt.component.scss']
})
export class MattypemgmtComponent implements OnInit {
  @Input() mattypes$: any | undefined;
  @Output() customChange = new EventEmitter<any>();
  
  constructor(private router: Router,private materialsService: MaterialsService, private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) { }

  ngOnInit(): void {
  
  }
 
  newMatType() {

    openEditCourseDialog(this.dialog, 'test').pipe(
      filter(val => !!val)
    )
      .subscribe(
        val => { console.log('Form value:', val);this.refresh(val);
         
          }
      );

  }
  refresh(val:any){
     this.materialsService.saveMatType(val);
     this.materialsService.getAllMaterieltype().subscribe(mattypes => {this.mattypes$ = mattypes;
      let stringifiedData = JSON.stringify(mattypes);  
      console.log("With Stringify :" , stringifiedData); 
      this.update(mattypes);
    
    });
  }
  update(val:any){
    this.customChange.emit(val);
  }
  loadData(){
    
  }
}
