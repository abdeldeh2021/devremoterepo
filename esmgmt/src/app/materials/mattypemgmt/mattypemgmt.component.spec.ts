import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MattypemgmtComponent } from './mattypemgmt.component';

describe('MattypemgmtComponent', () => {
  let component: MattypemgmtComponent;
  let fixture: ComponentFixture<MattypemgmtComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MattypemgmtComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MattypemgmtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
