import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialsRoutingModule } from './materials-routing.module';
import { MaterialsComponent } from './materials.component';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { EditmatComponent } from './editmat/editmat.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatDialogModule } from '@angular/material/dialog';

import {MatNativeDateModule} from '@angular/material/core';
import { MattypemgmtComponent } from './mattypemgmt/mattypemgmt.component';
import { MattypedialogComponent } from './mattypedialog/mattypedialog.component';
import { MaterielDetailsComponent } from './materiel-details/materiel-details.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { AuthInterceptorProvider } from '../utils/AuthInterceptorService';
import { MatcostdetailsComponent } from './matcostdetails/matcostdetails.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";




@NgModule({
  declarations: [
    MaterialsComponent,
    EditmatComponent,
    MattypemgmtComponent,
    MattypedialogComponent,
    MaterielDetailsComponent,
    MatcostdetailsComponent,


  ],
  imports: [
    CommonModule,
    MaterialsRoutingModule,
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,
    MatSelectModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatGridListModule,
    MatCardModule,
    MatPaginatorModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,


  ],
  exports: [
    EditmatComponent,
    MattypemgmtComponent,
    MattypedialogComponent,
    MaterielDetailsComponent,
    MatcostdetailsComponent
  ]
})
export class MaterialsModule { }
