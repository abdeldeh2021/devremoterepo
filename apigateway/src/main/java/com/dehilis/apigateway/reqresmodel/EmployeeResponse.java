package com.dehilis.apigateway.reqresmodel;

import java.util.List;

import com.dehilis.apigateway.model.Employee;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeResponse {
	private List<Employee> employees;
	private int totalPages;
	private int pageNumber;
	private int totalAmount;
	private int itemsPerPage;
	private int itemCount;
	public EmployeeResponse(List<Employee> employees, int totalPages, int pageNumber, int totalAmount, int itemsPerPage,
			int itemCount) {
		super();
		this.employees = employees;
		this.totalPages = totalPages;
		this.pageNumber = pageNumber;
		this.totalAmount = totalAmount;
		this.itemsPerPage = itemsPerPage;
		this.itemCount = itemCount;
	}
	

}
