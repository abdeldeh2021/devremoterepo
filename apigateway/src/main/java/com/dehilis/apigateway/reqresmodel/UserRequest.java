package com.dehilis.apigateway.reqresmodel;

import java.util.Collection;

import com.dehilis.apigateway.model.Role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {
    private String name;
    private Long id;

    private String username;

    private boolean isEnabled;

    private Collection<Role> roles ;

    private String Address;

    private String joiningDate;

    private String password;

}