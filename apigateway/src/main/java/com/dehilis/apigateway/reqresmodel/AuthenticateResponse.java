package com.dehilis.apigateway.reqresmodel;

import lombok.*;

@Getter
@AllArgsConstructor
public class AuthenticateResponse {
    private final String token;
}