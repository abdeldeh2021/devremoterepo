package com.dehilis.apigateway.reqresmodel;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class StringResponse {
    private String response;
}
