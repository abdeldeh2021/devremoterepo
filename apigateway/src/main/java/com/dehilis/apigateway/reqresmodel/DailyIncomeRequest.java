package com.dehilis.apigateway.reqresmodel;



import java.sql.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DailyIncomeRequest {

    private Date buildDate;
    @NotNull()
    private String userName;
    @Min(value=0)
    @Max(value=100)
    private long dailyIncome;
    @NotBlank
    private String dailyReport;
    
    
}
