package com.dehilis.apigateway.reqresmodel;

import com.dehilis.apigateway.repository.*;
import lombok.*;

import java.util.*;

@Getter
@Setter
@AllArgsConstructor
public class IncomesByMonth {
    List<IincomeCount> IincomeCounts;
}
