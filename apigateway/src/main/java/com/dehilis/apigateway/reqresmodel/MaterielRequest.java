package com.dehilis.apigateway.reqresmodel;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MaterielRequest {

    private long id;
    private String name;
    private long typeId;
    private boolean isEnabled;
    private String description;
    private String immat;
    private String assur;
    private Date dateAssur;
    private long projectId;
}
