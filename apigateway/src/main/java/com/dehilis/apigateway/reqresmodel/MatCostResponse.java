package com.dehilis.apigateway.reqresmodel;

import com.dehilis.apigateway.dto.MatCostDTO;
import com.dehilis.apigateway.model.*;
import lombok.*;

import java.util.*;


@Getter
@Setter
public class MatCostResponse {

	private List<MatCostDTO> matCosts;
	private int totalPages;
	private int pageNumber;
	private int totalAmount;
	private int itemsPerPage;
	private int itemCount;


	public MatCostResponse(List<MatCostDTO> matCosts, int totalPages, int pageNumber, int totalAmount, int itemsPerPage, int itemCount) {
		this.matCosts  = matCosts;
		this.totalPages = totalPages;
		this.pageNumber = pageNumber;
		this.totalAmount = totalAmount;
		this.itemsPerPage= itemsPerPage;
		this.itemCount=itemCount;
	}


}
