package com.dehilis.apigateway.reqresmodel;

import java.util.List;

import com.dehilis.apigateway.model.DailyIncome;
import lombok.*;


@Getter
@Setter
public class DailyIncomeResponse {
	
	private List<DailyIncome> dailyIncomes;
	private int totalPages;
	private int pageNumber;
	private int totalAmount;
	private int itemsPerPage;
	private int itemCount;


	public DailyIncomeResponse(List<DailyIncome> dailyIncomes, int totalPages,int pageNumber, int totalAmount, int itemsPerPage, int itemCount) {
		this.dailyIncomes  = dailyIncomes;
		this.totalPages = totalPages;
		this.pageNumber = pageNumber;
		this.totalAmount = totalAmount;
		this.itemsPerPage= itemsPerPage;
		this.itemCount=itemCount;
	}


}
