package com.dehilis.apigateway.reqresmodel;

import java.math.BigDecimal;
import java.sql.Date;

import com.dehilis.apigateway.model.Materiel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Setter
@Getter
public class MatCostReq {
    BigDecimal amount;

    private Materiel materiel;

    private Date insertDate;

    private Date costDate;
}
