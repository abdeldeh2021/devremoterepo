package com.dehilis.apigateway.reqresmodel;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class UserCreationRequest {
    private String username;
    private String password;
    //private Roles role;
    private String name;
}