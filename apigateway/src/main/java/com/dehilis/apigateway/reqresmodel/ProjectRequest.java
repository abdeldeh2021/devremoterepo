package com.dehilis.apigateway.reqresmodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProjectRequest {
	    private String name;
	    private String description;
	    private String location;
	    private String geolocation;
	    private String partnername;
	    private boolean isActive;
}
