package com.dehilis.apigateway.reqresmodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MatTypeRequest {

    private long id;
    private String name;
    private String description;
}
