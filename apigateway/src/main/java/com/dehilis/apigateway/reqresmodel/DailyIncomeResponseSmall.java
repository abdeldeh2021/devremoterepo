package com.dehilis.apigateway.reqresmodel;

import java.util.List;

import com.dehilis.apigateway.model.DailyIncome;
import lombok.*;


@Getter
@Setter
@AllArgsConstructor
public class DailyIncomeResponseSmall {
	private List<DailyIncome> dailyIncomes;
	
}
