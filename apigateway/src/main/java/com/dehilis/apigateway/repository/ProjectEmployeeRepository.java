package com.dehilis.apigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.ProjectEmployee;

@Service
public interface ProjectEmployeeRepository extends JpaRepository<ProjectEmployee,Long>  {

}
