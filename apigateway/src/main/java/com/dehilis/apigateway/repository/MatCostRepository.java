package com.dehilis.apigateway.repository;

import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.dehilis.apigateway.dto.MatCostDTO;
import com.dehilis.apigateway.model.MatCost;

@Repository
public interface MatCostRepository  extends JpaRepository<MatCost,Long> {

    @Query(value= "SELECT new  com.dehilis.apigateway.dto.MatCostDTO(m.id ,m.amount,m.insertDate,m.costDate,m.description,m.materiel.name,m.matCostType,m.materiel.id) FROM MatCost m  ")
    public Page<MatCostDTO> findAllWithMatName( Pageable pageRequest);
    @Query(value= "SELECT new  com.dehilis.apigateway.dto.MatCostDTO(m.id ,m.amount,m.insertDate,m.costDate,m.description,m.materiel.name,m.matCostType,m.materiel.id) FROM MatCost m WHERE m.materiel.id=?1 ")
    public Page<MatCostDTO> findAllWithMatId( Long matId ,Pageable pageRequest);

}
