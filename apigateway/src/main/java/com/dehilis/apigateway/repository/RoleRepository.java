package com.dehilis.apigateway.repository;

import com.dehilis.apigateway.model.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.*;

@Repository
public interface  RoleRepository  extends JpaRepository<Role,Long> {
    public Role findRoleByName(String name);
}
