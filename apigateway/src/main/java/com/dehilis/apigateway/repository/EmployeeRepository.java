package com.dehilis.apigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.Employee;

@Service
public interface EmployeeRepository extends JpaRepository<Employee,Long>  {

}
