package com.dehilis.apigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.Project;

@Service
public interface ProjectRepository extends JpaRepository<Project,Long>  {

}
