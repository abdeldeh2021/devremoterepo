package com.dehilis.apigateway.repository;

public interface IincomeCount {
    String getMonth();
    Long getIncomes();

}
