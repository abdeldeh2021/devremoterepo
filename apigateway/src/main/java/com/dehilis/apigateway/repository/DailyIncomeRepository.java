package com.dehilis.apigateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.DailyIncome;

@Service
public interface DailyIncomeRepository extends JpaRepository<DailyIncome,Long>  {
	public List<DailyIncome> findAllByOrderByBuildDateDesc();
	@Query(value="select to_char(d.income_date,'Mon') as month, sum(d.daily_income) as incomes from dailyincome  d group by month", nativeQuery = true)
	public List<IincomeCount> countIncomesByMonthInterface();

}
