package com.dehilis.apigateway.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dehilis.apigateway.dto.MaterielDTO;
import com.dehilis.apigateway.model.Materiel;

@Repository
public interface MaterielRepository extends JpaRepository<Materiel,Long> {
    @Query(
            value = "SELECT * FROM Materiels m WHERE m.mattype_id = ?1",
            nativeQuery = true)
    List<Materiel> getAllMaterielsByMattype(Long id);

    @Transactional(readOnly = true)
    @Query(value= "SELECT new  com.dehilis.apigateway.dto.MaterielDTO(m.id ,m.name,m.anneeConst,m.isEnabled,m.description,m.immat,m.assur,m.dateAssur,m.matType,m.esProject)  FROM Materiel m WHERE m.id = ?1 ")
    public MaterielDTO findMaterielBigById( Long id);

}
