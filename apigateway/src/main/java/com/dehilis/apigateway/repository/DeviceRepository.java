package com.dehilis.apigateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.CEDevice;

@Service
public interface DeviceRepository extends JpaRepository<CEDevice,Integer>  {

}
