package com.dehilis.apigateway.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.dehilis.apigateway.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User , Long>, JpaSpecificationExecutor<User>{

	public User findByUsername(String username);

}
