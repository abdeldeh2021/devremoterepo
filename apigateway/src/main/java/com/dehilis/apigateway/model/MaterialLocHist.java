package com.dehilis.apigateway.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import java.sql.*;

//Material location history
@Entity
@Table(name="matlochist")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MaterialLocHist {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id ;

    @Column(name="mat_id")
    private long matId;
    @Column(name="project_id")
    private long projectId;
    @Column(name="transferdate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date transferDate;
    @Column(name="user_name",nullable = false)
    private String userName;

    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @Column(nullable = false)
    private java.util.Date  insertDate;

    @PrePersist
    private void onCreate() {
        long now = System.currentTimeMillis();
        insertDate = new Date(now);
    }

}
