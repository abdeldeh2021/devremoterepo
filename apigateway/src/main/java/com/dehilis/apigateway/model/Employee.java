package com.dehilis.apigateway.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "employees")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(value = { "projectEmployees" })
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_generator")
	@SequenceGenerator(name = "employee_generator", sequenceName = "employee_seq")
	@Column(name = "employee_id")
	private long id;

	@Column(name = "firstname")
	String firstName;
	@Column(name = "lasttname")
	String lastName;
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "insert_date", nullable = false)
	private java.util.Date insertDate;
	@Column(name = "assurance")
	private String assur;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_assur")
	private Date dateAssur;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "date_birth")
	private Date dateOfBirth;
	@Column(name = "address")
	private String address;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "empl_date")
	private Date emplDate;
	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "isEnabled", columnDefinition = "boolean default false")
	private boolean isEnabled=false;
	@JsonFormat(pattern = "yyyy-MM-dd")
	@Column(name = "leave_date")
	private Date leaveDate;

	@OneToMany(mappedBy = "employee", fetch = FetchType.LAZY)
	private Set<ProjectEmployee> projectEmployees = new HashSet<>();

	@PrePersist
	private void onCreate() {
		long now = System.currentTimeMillis();
		insertDate = new Date(now);
	}

//    @ManyToMany(fetch = FetchType.LAZY,
//            cascade = {
//                    CascadeType.PERSIST,
//                    CascadeType.MERGE
//            },
//            mappedBy = "employees")
//    @JsonIgnore
//    private Set<EsProject> projects = new HashSet<>();

}
