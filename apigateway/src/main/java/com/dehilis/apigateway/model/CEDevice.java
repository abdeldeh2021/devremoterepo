package com.dehilis.apigateway.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import  javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="device")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CEDevice {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name = "id")
	private long id ;
	
	@Column(name="device_id",nullable = false)
	private String  devId;
	@Column(name="build_date")
	private Date buildDate;
	@Column(name="builder_name",nullable = false)
	private String builderName;
	@Column(name="daily_income" ,columnDefinition="Decimal(10,2) default '00.00'")
	private double dailyIncome;
	
}


