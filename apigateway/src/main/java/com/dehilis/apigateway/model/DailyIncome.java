package com.dehilis.apigateway.model;

import java.sql.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.*;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="dailyincome")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DailyIncome {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	@Column(name = "id")
	private long id ;
	
	@Column(name="income_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date buildDate;
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(nullable = false)
	private java.util.Date  insertDate;
	@Column(name="user_name",nullable = false)
	private String userName;
	@Column(name="daily_income",nullable = false)
	private long dailyIncome;
	@Column(name="day_report",nullable = true)
	private String dailyReport;

	@PrePersist
	private void onCreate() {
		long now = System.currentTimeMillis();
		insertDate = new Date(now);
	}

}
