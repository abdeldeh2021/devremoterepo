package com.dehilis.apigateway.model;

import java.sql.Date;

import java.util.*;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="users")
public class User {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private long id;
    @Column(name="name")
    private String name;
    @Column(name="isEnabled")
    private boolean isEnabled;

    @Column(nullable = true)
    private String address;
    @Column(nullable = true)
    private Date joiningDate;
    @Column(name="password")
    private String password;
    @Column(name="username" ,unique = true)
    private String username;

    @ManyToMany
    private Collection<Role> roles = new ArrayList<>();
//
//    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,
//            cascade = CascadeType.ALL)
//    private Set<Timesheet> timesheets;

  
}

    

