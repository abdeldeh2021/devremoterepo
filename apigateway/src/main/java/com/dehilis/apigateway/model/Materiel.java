package com.dehilis.apigateway.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name="materiels")
public class Materiel {
    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="auto_generator")
    private long id;
    @Column(name="name")
    private String name;
    @Column(name="annee_const")
    private Date anneeConst;
    @Column(name="isEnabled", columnDefinition = "boolean default true")
    private boolean isEnabled;
    @Column(name="description")
    private String description;
    @Column(name="immat")
    private String immat;
    @Column(name="assurance")
    private String assur;
    @JsonFormat(pattern="yyyy-MM-dd")
    @Column(name="date_assur")
    private Date dateAssur;
    @ManyToOne(fetch = FetchType.EAGER,optional = false)
    @JoinColumn(name="mattype_id",nullable = false)
    private MatType matType;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="project_id",nullable = true)
    private Project esProject;



}
