package com.dehilis.apigateway.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
public class MonthlyIncome {

    String month;
    long monthlyIncome;
}
