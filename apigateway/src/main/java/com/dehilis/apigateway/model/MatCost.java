package com.dehilis.apigateway.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.springframework.format.annotation.*;

import javax.persistence.*;
import java.math.*;
import java.sql.*;

@Entity
@Table(name="matcost")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MatCost {
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    @Column(name = "id")
    private long id ;
    
    @Column(name="description")
    private String description;
    
    @Column(name="amount",nullable = false)
    BigDecimal amount;
    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name="materiel_id",nullable = false)
    private Materiel materiel;
    
    @Temporal(TemporalType.DATE)
    @Column(name="insert_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private java.util.Date insertDate;
    
    @Column(name="cost_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date costDate;
    
    @Column(name="costtype")
    private String matCostType;

    @PrePersist
    private void onCreate() {
        long now = System.currentTimeMillis();
        insertDate = new Date(now);
    }
}
