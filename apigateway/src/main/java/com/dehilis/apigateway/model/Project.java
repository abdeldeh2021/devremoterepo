package com.dehilis.apigateway.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name="projects")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonIgnoreProperties(value= {"projectEmployees"})
public class Project {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy=GenerationType.SEQUENCE,generator="auto_generator")
    private long id;
    @Column(name="name")
    private String name;
    @Column(name="description")
    private String description;
    @Column(name="location")
    private String location;
    @Column(name="geolocation")
    private String geolocation;
    @Column(name="partnername")
    private String partnername;
    @Column(name="isActive", columnDefinition = "boolean default true")
    private boolean isActive;
    
    @OneToMany(mappedBy = "project",fetch = FetchType.LAZY, cascade=CascadeType.ALL)
    private Set<ProjectEmployee> projectEmployees= new HashSet<>();

//    @ManyToMany(fetch = FetchType.LAZY,
//            cascade = {
//                    CascadeType.PERSIST,
//                    CascadeType.MERGE
//            })
//    @JoinTable(name = "project_employee",
//            joinColumns = { @JoinColumn(name = "project_id") },
//            inverseJoinColumns = { @JoinColumn(name = "employee_id") })
//    private List<EsProject> employees ;

}
