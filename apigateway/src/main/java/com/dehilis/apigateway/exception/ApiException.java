package com.dehilis.apigateway.exception;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

public class ApiException {
    private String message;
    private Throwable throwable;
    private HttpStatus httpSatus;
    private ZonedDateTime zonedDateTime;


    public ApiException(String message, Throwable throwable, HttpStatus httpSatus, ZonedDateTime zonedDateTime) {
        super();
        this.message = message;
        this.throwable = throwable;
        this.httpSatus = httpSatus;
        this.zonedDateTime = zonedDateTime;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }

    public void setHttpSatus(HttpStatus httpSatus) {
        this.httpSatus = httpSatus;
    }

    public void setZonedDateTime(ZonedDateTime zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public HttpStatus getHttpSatus() {
        return httpSatus;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }
}
