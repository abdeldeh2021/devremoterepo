package com.dehilis.apigateway.exception;

public class ApiRequestException extends RuntimeException {
    /**
     * Exceptions
     */
    private static final long serialVersionUID = 1L;
    public ApiRequestException() {
        super();
    }
    public ApiRequestException(String message) {
        super(message);
    }
    public ApiRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}