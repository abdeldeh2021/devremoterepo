package com.dehilis.apigateway.controller;

import java.util.List;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.model.DailyIncome;
import com.dehilis.apigateway.repository.DailyIncomeRepository;
import com.dehilis.apigateway.repository.IincomeCount;
import com.dehilis.apigateway.reqresmodel.DailyIncomeRequest;
import com.dehilis.apigateway.reqresmodel.DailyIncomeResponse;
import com.dehilis.apigateway.reqresmodel.DailyIncomeResponseSmall;
import com.dehilis.apigateway.reqresmodel.StringResponse;
import com.dehilis.apigateway.service.imp.DailyIncomeServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/dailyincome")
public class DailyIncomeController {

	@Autowired
	DailyIncomeRepository dailyIncomeRepository;
	@Autowired
	DailyIncomeServiceImpl dailyIncomeService;
	@Autowired
	ModelMapper modelMapper;

	@PostMapping
	public ResponseEntity<StringResponse> addDailyIncome(@RequestBody DailyIncomeRequest dailyIncomeRequest) {

		DailyIncome dailyIncome = modelMapper.map(dailyIncomeRequest, DailyIncome.class);
		dailyIncomeService.save(dailyIncome);
		return ResponseEntity.ok(new StringResponse("Dailyincome record added "));

	}

	@GetMapping("/allwithoutpage")
	public DailyIncomeResponseSmall getAll() {
		return new DailyIncomeResponseSmall(dailyIncomeRepository.findAllByOrderByBuildDateDesc());
	}

	@GetMapping("/incomesmounth")
	public ResponseEntity<List<IincomeCount>> getAllIncomesMonth() {
		return ResponseEntity.ok(dailyIncomeRepository.countIncomesByMonthInterface());
	}

	@GetMapping
	public DailyIncomeResponse getAllDailyIncomes(@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "10") int size) {

		PageRequest pageRequest = PageRequest.of(page, size, Sort.by("buildDate").descending());
		Page<DailyIncome> pageResult = dailyIncomeService.getAllDailyIncome(pageRequest);
		return new DailyIncomeResponse(pageResult.getContent(), pageResult.getTotalPages(), pageResult.getNumber(),
				(int) pageResult.getTotalElements(), pageResult.getSize(), pageResult.getNumberOfElements());

	}

	@DeleteMapping("{id}")
	public ResponseEntity<StringResponse> deleteDailyIncomeById(@PathVariable("id") long id) {
		log.info(String.valueOf(id));
		dailyIncomeService.deleteDailyIncomeById(id);
		return ResponseEntity.ok(new StringResponse("Daily income record deleted"));
	}

}
