package com.dehilis.apigateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.exception.ApiRequestException;
import com.dehilis.apigateway.model.User;
import com.dehilis.apigateway.reqresmodel.AuthenticateRequest;
import com.dehilis.apigateway.reqresmodel.AuthenticateResponse;
import com.dehilis.apigateway.reqresmodel.UserCreationRequest;
import com.dehilis.apigateway.security.CustomUserDetailsService;
import com.dehilis.apigateway.security.JwtUtilService;
import com.dehilis.apigateway.service.UserService;


@RestController
@RequestMapping("/authenticate")
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private CustomUserDetailsService customUserDetailsService;
    @Autowired
    private JwtUtilService jwtUtilService;
    @Autowired
    private UserService userService;

    @PostMapping("/signIn")
    public ResponseEntity<AuthenticateResponse> generateAuthenticationToken(@RequestBody AuthenticateRequest authenticateRequest) throws ApiRequestException {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticateRequest.getUserName(),authenticateRequest.getPassword()));
        }
        catch (BadCredentialsException badCredentialsException) {
         throw  new ApiRequestException("Wrong UserName Password");
        }
        final UserDetails userDetails= customUserDetailsService.loadUserByUsername(authenticateRequest.getUserName());
        final String jwt=jwtUtilService.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticateResponse(jwt));
    }

    @PostMapping("/signUp")
    public ResponseEntity<User> createUser(@RequestBody UserCreationRequest userCreationRequest) {
      User user= userService.createUser(userCreationRequest);
      
      return ResponseEntity.ok(user);

    }

    

}