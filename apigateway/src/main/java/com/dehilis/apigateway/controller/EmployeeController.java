package com.dehilis.apigateway.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.dto.EmployeeDTO;
import com.dehilis.apigateway.exception.ApiRequestException;
import com.dehilis.apigateway.model.Employee;
import com.dehilis.apigateway.model.ProjectEmployee;
import com.dehilis.apigateway.reqresmodel.EmployeeResponse;
import com.dehilis.apigateway.reqresmodel.StringResponse;
import com.dehilis.apigateway.service.imp.EmployeeServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("v1/api/employee")
public class EmployeeController {

	@Autowired
	EmployeeServiceImpl employeeService;
	@Autowired
	MessageSource messages;

	@GetMapping
	public EmployeeResponse getAllEmployees(@RequestParam(name = "page", defaultValue = "0") int page,
			@RequestParam(name = "size", defaultValue = "10") int size) {

		PageRequest pageRequest = PageRequest.of(page, size, Sort.by("firstName").descending());
		Page<Employee> pageResult = employeeService.findAllEmployees(pageRequest);
		return new EmployeeResponse(pageResult.getContent(), pageResult.getTotalPages(), pageResult.getNumber(),
				(int) pageResult.getTotalElements(), pageResult.getSize(), pageResult.getNumberOfElements());

	}

	@PostMapping
	public ResponseEntity<StringResponse> createEmployee(@RequestBody EmployeeDTO employeeDTO) {
		log.debug("REST request to save Employee : {}", employeeDTO);
		if(employeeDTO.getId()!=null) {
			throw new ApiRequestException("A new employee cannot already have an ID : id exists ");
		}
		Employee employee = employeeService.createEmployee(employeeDTO);
		String message= String.format(messages.getMessage("employeeproject.create.message", null, Locale.forLanguageTag("fr-FR")), employee.getId());
		return ResponseEntity.ok(new StringResponse(message + employee.toString()));
		
	}

	@PostMapping("/assignproject")
	public ResponseEntity<ProjectEmployee> assignProjecToEmp(@RequestParam(name = "empId") long empId,
			@RequestParam(name = "projectId") long projectId,
			@RequestHeader(value = "Accept-Language", required = false) Locale locale) {
		return ResponseEntity.ok(employeeService.assignProjectToEmployee(empId, projectId, locale));

	}

	@DeleteMapping(value = "/{organizationId}")
	public ResponseEntity<String> deleteProjectEmployee(@PathVariable("id") Long id) {
		return ResponseEntity.ok(employeeService.delete(id));
	}

}
