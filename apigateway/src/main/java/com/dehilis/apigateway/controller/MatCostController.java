package com.dehilis.apigateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.dto.MatCostDTO;
import com.dehilis.apigateway.exception.ApiRequestException;
import com.dehilis.apigateway.model.MatCost;
import com.dehilis.apigateway.reqresmodel.MatCostResponse;
import com.dehilis.apigateway.reqresmodel.StringResponse;
import com.dehilis.apigateway.service.imp.MatCostServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/matcost")
public class MatCostController {

    @Autowired
    MatCostServiceImpl  matCostService;
    @GetMapping
    public MatCostResponse getAllDailyIncomes(@RequestParam(name = "page", defaultValue = "0") int page,
                                                  @RequestParam(name = "size", defaultValue = "10") int size) {

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("insertDate").descending());
        Page<MatCostDTO> pageResult = matCostService.findAllWithMatName(pageRequest);
        return new MatCostResponse(pageResult.getContent(), pageResult.getTotalPages(), pageResult.getNumber(),
                (int) pageResult.getTotalElements(), pageResult.getSize(), pageResult.getNumberOfElements());

    }
    @GetMapping("/matcostsbyid")
    public MatCostResponse findAllByMatIdPerPage(@RequestParam(name = "page", defaultValue = "0") int page,
                                      @RequestParam(name = "size", defaultValue = "10") int size,  @RequestParam(name = "matId") Long matId){
        log.info("inside /matcostsbyid");
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by("costDate").descending());
        Page<MatCostDTO> pageResult =  matCostService.findAllWithMatId(matId ,pageRequest);
        return new MatCostResponse(pageResult.getContent(), pageResult.getTotalPages(), pageResult.getNumber(),
                (int) pageResult.getTotalElements(), pageResult.getSize(), pageResult.getNumberOfElements());

    }
    @PostMapping
    public ResponseEntity<StringResponse> addMatCost(@RequestBody MatCost matCost) {
        try {
            matCostService.save(matCost);
            log.info(String.valueOf(matCost));
            return  ResponseEntity.ok(new StringResponse("Record matCost created ") );
        }catch (Exception e) {
            throw new ApiRequestException("Bad Request cannot add MatCost");
        }

    }
}
