package com.dehilis.apigateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.exception.ApiRequestException;
import com.dehilis.apigateway.reqresmodel.ProjectRequest;
import com.dehilis.apigateway.reqresmodel.StringResponse;
import com.dehilis.apigateway.service.imp.ProjectServiceImpl;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("v1/api/project")
public class ProjectController {

	@Autowired
	ProjectServiceImpl projectService;

	@PostMapping
	public ResponseEntity<StringResponse> createProject(@RequestBody ProjectRequest projectRequest) {
		try {
			log.info("adding project "+ projectRequest);
			projectService.createProject(projectRequest);
			return ResponseEntity.ok(new StringResponse("project created... "));
		} catch (Exception e) {
			throw new ApiRequestException("Bad Request!");
		}
	}
}
