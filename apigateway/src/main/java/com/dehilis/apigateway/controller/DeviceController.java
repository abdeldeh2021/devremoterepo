package com.dehilis.apigateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.dto.DeviceDTO;
import com.dehilis.apigateway.model.CEDevice;
import com.dehilis.apigateway.repository.DeviceRepository;

@RestController
@RequestMapping("/api/readdevices")
public class DeviceController {
	
	private List<CEDevice> devices = new  ArrayList<>();
	
	@Autowired
	DeviceRepository deviceRepository;
	@PostMapping
	public ResponseEntity<String> addCEdevice(@RequestBody DeviceDTO deviceDTO) {
		
		//TODO create  Service for CEDevices
//		deviceRepository.save(deviceDTO);
//		devices.add(deviceDTO);
		return  ResponseEntity.status(HttpStatus.OK)
		        .body("CE device inserted " +deviceDTO.getDevId());
	}
	@GetMapping
	public List<CEDevice> getAllDevices(){
		return deviceRepository.findAll();
	}
	@GetMapping("/status")
	public String getStatus(){
		return "Listening";
	}

}
