package com.dehilis.apigateway.controller;


import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dehilis.apigateway.dto.MaterielDTO;
import com.dehilis.apigateway.model.MatType;
import com.dehilis.apigateway.model.Materiel;
import com.dehilis.apigateway.reqresmodel.MatTypeRequest;
import com.dehilis.apigateway.reqresmodel.MaterielRequest;
import com.dehilis.apigateway.service.MaterielService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/materiel")
public class MaterielController {

    @Autowired
    MaterielService materielService;
    @Autowired
    ModelMapper modelMapper;

    @GetMapping("/mattypes")
    public List<MatTypeRequest> getAllMaterielTypes() {
        return materielService.getAllMaterielTypes().stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MatType> createMatType(@RequestBody MatType matType) {
        return ResponseEntity.ok(materielService.createMaterielType(matType));
    }

    @GetMapping("/matbytype/{id}")
    public List<MaterielRequest> getAllMaterielPerTyp(@PathVariable long id) {
        // List<Materiel> materiels =  materielService.getAllMaterielsByMattype(id);
        return materielService.getAllMaterielsByMattype(id).stream().map(this::convertToDtoMat).collect(Collectors.toList());

    }

    @PostMapping("/creatematerial")
    public ResponseEntity<Materiel> createMateriel(@RequestBody MaterielRequest materielRequest) {
        return ResponseEntity.ok(materielService.createMateriel(materielRequest));
    }

    

    @GetMapping("/{id}")
    public ResponseEntity<MaterielDTO> getAllMaterielById(@PathVariable long id) {
        // List<Materiel> materiels =  materielService.getAllMaterielsByMattype(id);
        //return materielService.getAllMaterielsByMattype(id).stream().map(this::convertToDtoMat).collect(Collectors.toList());
        MaterielDTO materielDTO = materielService.getMaterielById(id);
        log.info(String.valueOf(materielDTO.getEsProject().getId()));
        return ResponseEntity.ok(materielDTO);
    }
    private MatTypeRequest convertToDto(MatType matType) {
        return this.modelMapper.map(matType, MatTypeRequest.class);
    }

    private MaterielRequest convertToDtoMat(Materiel materiel) {
        return this.modelMapper.map(materiel, MaterielRequest.class);
    }
}
