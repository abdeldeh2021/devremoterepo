package com.dehilis.apigateway.service;

import com.dehilis.apigateway.dto.MatCostDTO;
import com.dehilis.apigateway.model.*;
import org.springframework.data.domain.*;

public interface MatCostService {
    Page<MatCost> getAllMatCosts(PageRequest pageRequest);
    public Page<MatCostDTO> findAllWithMatName( Pageable pageRequest);
    public MatCost save(MatCost matCost);
    public Page<MatCostDTO> findAllWithMatId(Long matId,Pageable pageRequest);
}
