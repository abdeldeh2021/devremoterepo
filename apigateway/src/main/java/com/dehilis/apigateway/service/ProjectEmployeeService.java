package com.dehilis.apigateway.service;

import com.dehilis.apigateway.model.ProjectEmployee;



public interface ProjectEmployeeService {
	 public ProjectEmployee createProjectEmployee(ProjectEmployee projectEmployee);
     public String delete(ProjectEmployee projectEmployee);

}
