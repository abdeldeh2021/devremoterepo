package com.dehilis.apigateway.service;

import com.dehilis.apigateway.dto.MaterielDTO;
import com.dehilis.apigateway.model.*;
import com.dehilis.apigateway.reqresmodel.*;

import java.util.*;

public interface MaterielService {
    List<MatType> getAllMaterielTypes();
    MatType createMaterielType(MatType matType);
    MatType findMatTypeById(Long id);
    Materiel createMateriel(MaterielRequest materielRequest);
    List<Materiel> getAllMaterielsByMattype(Long id);
    MaterielDTO getMaterielById(Long id);
}
