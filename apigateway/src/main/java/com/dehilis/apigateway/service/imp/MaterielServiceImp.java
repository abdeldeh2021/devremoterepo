package com.dehilis.apigateway.service.imp;

import com.dehilis.apigateway.dto.MaterielDTO;
import com.dehilis.apigateway.model.*;
import com.dehilis.apigateway.repository.*;
import com.dehilis.apigateway.reqresmodel.*;
import com.dehilis.apigateway.service.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

@Slf4j
@Service
@Transactional
public class MaterielServiceImp implements MaterielService {
    @Autowired
    MaterielRepository materielRepository;
    @Autowired
    MaterielTypeRepository materielTypeRepository;

    @Transactional
    @Override
    public List<MatType> getAllMaterielTypes() {
        return materielTypeRepository.findAll();
    }


    @Override
    public MatType createMaterielType(MatType matType) {
        return materielTypeRepository.save(matType);
    }


    public MatType findMatTypeById(Long id) {
        Optional<MatType> matTypeOpt = materielTypeRepository.findById(id);
        MatType mat = null;
        try {
        	if(!matTypeOpt.isEmpty()) {
        		 mat = matTypeOpt.get();
        	}
        }catch (Exception e) {
			log.debug("Materiel Type not found..." + e.getMessage());
		}
        return mat;
    }

    @Override
    @Transactional
    public Materiel createMateriel(MaterielRequest materielRequest) {
        Materiel materiel = new Materiel();
        materiel.setName(materielRequest.getName());
        materiel.setDescription(materielRequest.getDescription());
        materiel.setAssur(materielRequest.getAssur());
        materiel.setEnabled(materielRequest.isEnabled());
        materiel.setDateAssur(materielRequest.getDateAssur());
        log.info(String.valueOf(materielRequest.getTypeId()));
        MatType matType = materielTypeRepository.getById(materielRequest.getTypeId());

        materiel.setMatType(matType);
        materielRepository.save(materiel);
        return materiel;
    }

    @Override
    public List<Materiel> getAllMaterielsByMattype(Long id) {
        return materielRepository.getAllMaterielsByMattype(id);
    }

    @Override
    public MaterielDTO getMaterielById(Long id) {
        return materielRepository.findMaterielBigById(id);
    }
}
