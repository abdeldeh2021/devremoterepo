package com.dehilis.apigateway.service.imp;


import java.util.Locale;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.dto.EmployeeDTO;
import com.dehilis.apigateway.model.Employee;
import com.dehilis.apigateway.model.Project;
import com.dehilis.apigateway.model.ProjectEmployee;
import com.dehilis.apigateway.repository.EmployeeRepository;
import com.dehilis.apigateway.repository.ProjectEmployeeRepository;
import com.dehilis.apigateway.repository.ProjectRepository;
import com.dehilis.apigateway.service.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	ProjectRepository projectRepository;
	
	@Autowired
	ProjectEmployeeRepository projectEmployeeRepository;
	
	@Autowired
	MessageSource messages;
	
	@Override
	public Page<Employee> findAllEmployees(Pageable pageRequest) {
		return employeeRepository.findAll(pageRequest);
	}
	@Transactional
	@Override
	public Employee createEmployee(EmployeeDTO employeeRequest) {
		Employee employee = new Employee();
		employee.setFirstName(employeeRequest.getFirstName());
		employee.setLastName(employeeRequest.getLastName());
		employee.setDateOfBirth(employeeRequest.getDateOfBirth());
		employee.setAssur(employee.getAssur());
		Employee savedEmp= employeeRepository.save(employee);
		
		if(employeeRequest.getProjectId()!=null) {
			Optional<Project> project = projectRepository.findById(employeeRequest.getProjectId());
			
			if(project.isPresent()) {
				ProjectEmployee pe = new ProjectEmployee();
				pe.setEmployee(savedEmp);
				pe.setProject(project.get());		
				pe.setAssignmentDate(new java.sql.Date(System.currentTimeMillis()));
				projectEmployeeRepository.save(pe);
			}
		}
		
		return savedEmp;
	}
	@Override
	public ProjectEmployee assignProjectToEmployee(long empId, long projectId, Locale locale) {
		String responseMessage = null;
		ProjectEmployee pe=null;
		Optional<Employee> employee = employeeRepository.findById(empId);
		Optional<Project> project = projectRepository.findById(projectId);
		if(employee.isPresent()&& project.isPresent()) {
			pe= new ProjectEmployee();
			pe.setEmployee(employee.get());
			pe.setProject(project.get());
			pe = projectEmployeeRepository.save(pe);
			//responseMessage = String.format(messages.getMessage("employeeproject.create.message",null,locale), pe.toString());
			
		}
		return pe;
	}
	@Override
	public String delete(long projectEmployeeId) {
		String responseMessage = null;
		projectEmployeeRepository.deleteById(projectEmployeeId);
		responseMessage = String.format(messages.getMessage("employeeproject.delete.message", null, null),projectEmployeeId);
		return responseMessage;
	}

}
