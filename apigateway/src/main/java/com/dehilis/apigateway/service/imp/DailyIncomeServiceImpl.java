package com.dehilis.apigateway.service.imp;

import com.dehilis.apigateway.exception.*;
import com.dehilis.apigateway.model.*;
import com.dehilis.apigateway.repository.*;
import com.dehilis.apigateway.service.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;

@Slf4j
@Service
public class DailyIncomeServiceImpl implements DailyIncomeService {
    @Autowired
    DailyIncomeRepository dailyIncomeRepository;
    @Override
    public Page<DailyIncome> getAllDailyIncome(PageRequest pageRequest) {
        return dailyIncomeRepository.findAll(pageRequest);
    }

    public void save(DailyIncome dailyIncome) {
        dailyIncomeRepository.save(dailyIncome);
    }
    @Override
    public void deleteDailyIncomeById(long id) {
        DailyIncome dailyIncome = dailyIncomeRepository.findById(id).orElseThrow(() -> new ApiRequestException("DailyIncome  "+ id +" not found"));
        log.info(String.valueOf(dailyIncome));
        dailyIncomeRepository.deleteById(id);
    }
}
