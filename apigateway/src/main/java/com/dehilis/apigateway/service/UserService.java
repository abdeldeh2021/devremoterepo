package com.dehilis.apigateway.service;

import com.dehilis.apigateway.model.User;
import com.dehilis.apigateway.reqresmodel.UserRequest;
import com.dehilis.apigateway.reqresmodel.UserCreationRequest;

public interface UserService {
	 public User getUserByUsername(String username);
	 public User createUser(UserCreationRequest userCreationRequest);
	 public User createUser(UserRequest employeeRequest);
	 public User updateUser(UserRequest employeeRequest, String username);
	 public void deleteUser(UserRequest employeeRequest);
	 public User updateEmployeeAdmin(UserRequest employeeRequest);

}
