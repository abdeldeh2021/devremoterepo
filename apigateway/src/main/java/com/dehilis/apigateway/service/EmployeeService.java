package com.dehilis.apigateway.service;

import java.util.Locale;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dehilis.apigateway.dto.EmployeeDTO;
import com.dehilis.apigateway.model.Employee;
import com.dehilis.apigateway.model.ProjectEmployee;



public interface EmployeeService {
	 public Page<Employee> findAllEmployees( Pageable pageRequest);
	 public Employee createEmployee(EmployeeDTO employeeRequest);
	 public ProjectEmployee assignProjectToEmployee(long empId, long projectId,  Locale locale);
	 public String delete(long projectEmployeeId);

}
