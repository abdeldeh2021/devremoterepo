package com.dehilis.apigateway.service;

import com.dehilis.apigateway.model.*;
import org.springframework.data.domain.*;

public interface DailyIncomeService {
    Page<DailyIncome>  getAllDailyIncome(PageRequest pageRequest);
    public void save(DailyIncome dailyIncome);
    public void deleteDailyIncomeById(long id);
}
