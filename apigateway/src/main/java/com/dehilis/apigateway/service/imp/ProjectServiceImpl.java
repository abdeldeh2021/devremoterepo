package com.dehilis.apigateway.service.imp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.Project;
import com.dehilis.apigateway.repository.ProjectRepository;
import com.dehilis.apigateway.reqresmodel.ProjectRequest;
import com.dehilis.apigateway.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {

	@Autowired
	ProjectRepository projectRepository;
	@Override
	public Page<Project> findAllProjects(Pageable pageRequest) {
		return projectRepository.findAll(pageRequest);
	}
	@Transactional
	@Override
	public Project createProject(ProjectRequest projectRequest) {
		Project project = new Project();
		project.setName(projectRequest.getName());
		project.setPartnername(projectRequest.getPartnername());
		project.setActive(projectRequest.isActive());
		return  projectRepository.save(project);
		
	}
	

}
