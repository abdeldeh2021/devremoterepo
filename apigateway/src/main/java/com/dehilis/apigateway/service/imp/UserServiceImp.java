package com.dehilis.apigateway.service.imp;

import java.sql.Date;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dehilis.apigateway.model.Role;
import com.dehilis.apigateway.model.User;
import com.dehilis.apigateway.repository.UserRepository;
import com.dehilis.apigateway.reqresmodel.UserCreationRequest;
import com.dehilis.apigateway.reqresmodel.UserRequest;
import com.dehilis.apigateway.service.UserService;

@Service
@Transactional(readOnly = true)
public class UserServiceImp implements UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoderdEncoder;

	public User getUserByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Transactional
	public User createUser(UserCreationRequest userCreationRequest) {
		User user = new User();
		user.setEnabled(true);
		// user.setRole(userCreationRequest.getRole().name());
		user.setUsername(userCreationRequest.getUsername());
		user.setPassword(bCryptPasswordEncoderdEncoder.encode(userCreationRequest.getPassword()));
		user.setName(userCreationRequest.getName());
		return userRepository.save(user);

	}

	@Transactional
	public User createUser(UserRequest employeeRequest) {
		User user = new User();
		user.setEnabled(true);
		Role role = new Role(1, "test");
		Collection<Role> roles = employeeRequest.getRoles();
		user.getRoles().addAll(roles);
		// user.setRole(employeeRequest.getRole().name());
		user.setUsername(employeeRequest.getUsername());
		user.setPassword(bCryptPasswordEncoderdEncoder.encode(employeeRequest.getPassword()));
		user.setName(employeeRequest.getName());
		user.setAddress(employeeRequest.getAddress());
		user.setJoiningDate(Date.valueOf(employeeRequest.getJoiningDate()));
		return userRepository.save(user);

	}

	@Transactional
	public User updateUser(UserRequest employeeRequest, String username) {
		User user = userRepository.findByUsername(username);
		user.setEnabled(true);

		user.setUsername(employeeRequest.getUsername());
		if (employeeRequest.getPassword() != null) {
			user.setPassword(bCryptPasswordEncoderdEncoder.encode(employeeRequest.getPassword()));

		}
		user.setName(employeeRequest.getName());
		user.setAddress(employeeRequest.getAddress());
		user.setJoiningDate(Date.valueOf(employeeRequest.getJoiningDate()));
		return userRepository.save(user);
	}

//    public Page<User> getEmployees(PageInfo pageInfo)
//    {
//        Sort sort=Sort.by(pageInfo.getSortDirection(), pageInfo.getSortBy());
//        Pageable pageable= PageRequest.of(pageInfo.getPageNumber(), pageInfo.getPageSize(),sort);
//        return userRepository.findAll(pageable);
//    }
//
//    public Page<User> getEmployeesByCriteria(EmployeeFilterRequest employeeFilterRequest)
//    {
//        Sort sort=Sort.by(employeeFilterRequest.getSortDirection(),employeeFilterRequest.getSortBy());
//        Pageable pageable= PageRequest.of(employeeFilterRequest.getPageNumber(),employeeFilterRequest.getPageSize(),sort);
//        return userRepository.findAll(UserSpecs.getUserByIdUserNameNameAddress(employeeFilterRequest),pageable);
//    }

	public void deleteUser(UserRequest employeeRequest) {
		userRepository.deleteById(employeeRequest.getId());

	}

	public User updateEmployeeAdmin(UserRequest employeeRequest) {
		Optional<User> userOpt = userRepository.findById(employeeRequest.getId());
		if(userOpt.isPresent()) {
			User user= userOpt.get();
			user.setEnabled(true);
			user.setUsername(employeeRequest.getUsername());
			if (employeeRequest.getPassword() != null) {
				user.setPassword(bCryptPasswordEncoderdEncoder.encode(employeeRequest.getPassword()));

			}
			user.setName(employeeRequest.getName());
			user.setAddress(employeeRequest.getAddress());
			user.setJoiningDate(Date.valueOf(employeeRequest.getJoiningDate()));
			return userRepository.save(user);
		}
		return null;
		
	}

}