package com.dehilis.apigateway.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.dehilis.apigateway.model.Project;
import com.dehilis.apigateway.reqresmodel.ProjectRequest;



public interface ProjectService {
	 public Page<Project> findAllProjects( Pageable pageRequest);
	 public Project createProject(ProjectRequest projectRequest);

}
