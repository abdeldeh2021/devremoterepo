package com.dehilis.apigateway.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.dehilis.apigateway.dto.DeviceDTO;
import com.dehilis.apigateway.model.CEDevice;

@Mapper(componentModel = "spring")
public interface DeviceMapper {
	DeviceMapper INSTANCE = Mappers.getMapper( DeviceMapper.class );
	 
    
    DeviceDTO deviceToDeviceDto(CEDevice ceDevice); 
}


