package com.dehilis.apigateway.service.imp;

import com.dehilis.apigateway.dto.MatCostDTO;
import com.dehilis.apigateway.model.*;
import com.dehilis.apigateway.repository.*;
import com.dehilis.apigateway.service.*;
import lombok.extern.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.stereotype.*;

@Slf4j
@Service
public class MatCostServiceImpl implements MatCostService {

    @Autowired
    MatCostRepository matCostRepository;
    @Override
    public Page<MatCost> getAllMatCosts(PageRequest pageRequest) {
        return matCostRepository.findAll(pageRequest);
    }

    @Override
    public Page<MatCostDTO> findAllWithMatName(Pageable pageRequest) {
        return matCostRepository.findAllWithMatName(pageRequest);
    }
    @Override
    public Page<MatCostDTO> findAllWithMatId(Long matId,Pageable pageRequest) {
        return matCostRepository.findAllWithMatId(matId,pageRequest);
    }
    @Override
    public MatCost save(MatCost matCost) {

       MatCost matCost_1 =  matCostRepository.save(matCost);
       log.info(String.valueOf(matCost));
       return matCost_1;

    }
}
