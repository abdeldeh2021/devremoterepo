package com.dehilis.apigateway.security;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
    private CustomUserDetailsService customUserDetailsService;
    @Autowired
    private JwtUtilService jwtUtilService;
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
            final String authorizationHeader=httpServletRequest.getHeader("Authorization");
            String jwt=null;
            List<SimpleGrantedAuthority> role=null;
            String username=null;
            log.info(">>>>>inside filter" + authorizationHeader );
//            if(authorizationHeader!=null && authorizationHeader.startsWith("Bearer ")) {
//                jwt=authorizationHeader.substring(7);
//                username=jwtUtilService.extractUsername(jwt);
//                List<SimpleGrantedAuthority> roles = jwtUtilService.extractRole(jwt);
//                roles.forEach(role1->{log.info(String.valueOf(role1.getAuthority()));});
//                log.info(">>>>" + username);
//                role=jwtUtilService.extractRole(jwt);
//                //System.out.println(role.toArray()[0]);
//            }
//
//        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
//
//            UserDetails userDetails = this.customUserDetailsService.loadUserByUsername(username);
//
//            if (jwtUtilService.validateToken(jwt, userDetails)) {
//
//                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
//                        userDetails, null, userDetails.getAuthorities());
//                usernamePasswordAuthenticationToken
//                        .setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
//                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
//            }
//        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);

    }
}
