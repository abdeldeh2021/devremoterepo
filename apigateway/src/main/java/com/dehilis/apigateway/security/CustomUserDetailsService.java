package com.dehilis.apigateway.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dehilis.apigateway.model.User;
import com.dehilis.apigateway.service.imp.UserServiceImp;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class CustomUserDetailsService implements UserDetailsService{
	@Autowired
	private UserServiceImp userservice;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user=userservice.getUserByUsername(username);
		if(user != null && user.isEnabled()) {
			log.info("User found in database {}", username);
			List<GrantedAuthority> roleList=new ArrayList<>();
			log.info(username);
			//problem if i dont add role i will not get any response 
			//TODO revise this code
			roleList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			log.info(username);
			 
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),roleList);
		}
		else {
		        throw new UsernameNotFoundException("username not found");
		    }
	}
}
