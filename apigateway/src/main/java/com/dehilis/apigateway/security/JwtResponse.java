package com.dehilis.apigateway.security;

import lombok.*;

@Getter
@AllArgsConstructor
public class JwtResponse {
    private final String jwtToken;
}
