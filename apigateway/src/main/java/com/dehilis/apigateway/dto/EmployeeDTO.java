package com.dehilis.apigateway.dto;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {
	private Long id;
	private String firstName;
	private String lastName;
	private String assur;
	private Date dateAssur;
	private Date dateOfBirth;
	private String address;
	private Date emplDate;
	private String phoneNumber;
	private Long  projectId;
}
