package com.dehilis.apigateway.dto;

import java.math.BigDecimal;
import java.util.Date;



public class MatCostDTO implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
	private long id ;
    BigDecimal amount;
    private Date insertDate;
    private Date costDate;
    private String description;
    private String matname;

    private String matCostType;
    private Long matId;


    public MatCostDTO(long id, BigDecimal amount, Date insertDate,Date costDate, String description, String matname,  String matCostType, Long matId) {
        this.id = id;
        this.amount = amount;
        this.insertDate = insertDate;
        this.description = description;
        this.matname = matname;
        this.costDate = costDate;
        this.matCostType = matCostType;
        this.matId = matId;
    }




    public String getMatname() {
        return matname;
    }
    public void setMatname(String matname) {
        this.matname = matname;
    }

    public MatCostDTO() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public String getDescription() {
        return description;
    }

    public Date getCostDate() {
        return costDate;
    }

    public void setCostDate(Date costDate) {
        this.costDate = costDate;
    }

    public String getMatCostType() {
        return matCostType;
    }

    public void setMatCostType(String matCostType) {
        this.matCostType = matCostType;
    }

    public Long getMatId() {
        return matId;
    }

    public void setMatId(Long matId) {
        this.matId = matId;
    }
    //private String materielName;
}
