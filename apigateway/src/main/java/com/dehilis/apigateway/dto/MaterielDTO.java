package com.dehilis.apigateway.dto;

import java.io.Serializable;
import java.util.Date;

import com.dehilis.apigateway.model.MatType;
import com.dehilis.apigateway.model.Project;

import lombok.ToString;

@ToString
public class MaterielDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;
    private String name;
    private Date anneeConst;
    private boolean isEnabled;
    private String description;
    private String immat;
    private String assur;
    private Date dateAssur;
    private transient MatType matType;
    private transient Project esProject;

    public MaterielDTO() {
		super();
	}

 
    public MaterielDTO(long id, String name, Date anneeConst, boolean isEnabled, String description, String immat,
			String assur, Date dateAssur, MatType matType, Project esProject) {
		super();
		this.id = id;
		this.name = name;
		this.anneeConst = anneeConst;
		this.isEnabled = isEnabled;
		this.description = description;
		this.immat = immat;
		this.assur = assur;
		this.dateAssur = dateAssur;
		this.matType = matType;
		this.esProject = esProject;
	}
   

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getAnneeConst() {
        return anneeConst;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public String getDescription() {
        return description;
    }

    public String getImmat() {
        return immat;
    }

    public String getAssur() {
        return assur;
    }

    public Date getDateAssur() {
        return dateAssur;
    }

    public MatType getMatType() {
        return matType;
    }

    public Project getEsProject() {
        return esProject;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAnneeConst(Date anneeConst) {
        this.anneeConst = anneeConst;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImmat(String immat) {
        this.immat = immat;
    }

    public void setAssur(String assur) {
        this.assur = assur;
    }

    public void setDateAssur(Date dateAssur) {
        this.dateAssur = dateAssur;
    }

    public void setMatType(MatType matType) {
        this.matType = matType;
    }

    public void setEsProject(Project esProject) {
        this.esProject = esProject;
    }


	


	
}
